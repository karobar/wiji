# Wiji

Wiji is a terminal emulation library developed with roguelikes in mind.

# Creating a New Version

    ./gradlew jar
    
This will create a `Wiji.jar` in the `build/libs` directory.

# Publish a Version to artifactory

**Edit the version variable in gradle.properties, then:**

    ./gradlew artifactoryPublish
    
You may need to add Artifactory Credentials to get this to work by adding `artifactory_user`
and `artifactory_password` to `~/.gradle/gradle.properties`
