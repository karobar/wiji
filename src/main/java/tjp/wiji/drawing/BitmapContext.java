package tjp.wiji.drawing;

import com.badlogic.gdx.files.FileHandle;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import tjp.wiji.drawing.bootstrap.FilesModule;
import tjp.wiji.file.IncludedTileset;
import tjp.wiji.file.beans.TileSetDescription;
import tjp.wiji.representations.Graphic;
import tjp.wiji.representations.ImageRepresentation;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Objects;

/**
 * Represents the pixel width and height of a character and the charsheet that it uses.
 */
public class BitmapContext {
    public final static int DEFAULT_CHAR_PIXEL_WIDTH  = 8;
    public final static int DEFAULT_CHAR_PIXEL_HEIGHT = 12;
    public final static int CHARSHEET_GRID_HEIGHT = 16;

    public static final Color DEFAULT_CONTROL_FORECOLOR = Color.WHITE;
    public static final Color DEFAULT_CONTROL_COLOR = new Color(0xFF33236B);

    private final int charPixelWidth, charPixelHeight;
    private final FileHandle charsheet;
    private final Color controlColor;


    /**
     *
     * @param files an INITIALIZED Files instance
     * @return
     * @throws URISyntaxException
     */
    public static BitmapContext create(final FilesModule files) throws URISyntaxException {
        return new BitmapContext(files.internal("charsheet_8x12.bmp"));
    }

    /**
     * Defaults for char pixel height, width, and charsheet.
     * @throws URISyntaxException 
     */
    public BitmapContext(final FileHandle charsheet) {
        this(DEFAULT_CHAR_PIXEL_WIDTH, DEFAULT_CHAR_PIXEL_HEIGHT,
                charsheet);
    }

    public BitmapContext(final int charPixelWidth, final int charPixelHeight,
            final FileHandle charsheet) {
        this(charPixelWidth, charPixelHeight, charsheet, DEFAULT_CONTROL_COLOR);
    }

    /**
     * Not recommended, use the fromYaml loader instead
     * @param charPixelWidth
     * @param charPixelHeight
     * @param charsheet
     * @param controlColor
     */
    public BitmapContext(final int charPixelWidth, final int charPixelHeight,
                         final FileHandle charsheet, final Color controlColor) {
        this.charPixelWidth = charPixelWidth;
        this.charPixelHeight = charPixelHeight;
        this.charsheet = charsheet;
        this.controlColor = controlColor;
    }

    public static BitmapContext fromYaml(final String path, final FilesModule files)
            throws FileNotFoundException, URISyntaxException {
        URI uri = ClassLoader.getSystemClassLoader().getResource(path).toURI();
        File fileHelp = new File(Objects.requireNonNull(uri));
        assert(fileHelp.exists());
        return fromYaml(new FileHandle(fileHelp), files);
    }

    public static BitmapContext fromYaml(final FileHandle yamlHandle, final FilesModule files) throws FileNotFoundException,
            URISyntaxException {
        if (yamlHandle.exists()) {
            final InputStream stream = new FileInputStream(yamlHandle.file());

            final Yaml processor = new Yaml(new Constructor(TileSetDescription.class));
            final TileSetDescription loaded = processor.load(stream);

            final URL charsheetUrl = ClassLoader.getSystemClassLoader().getResource(
                    loaded.getFilename());
            final FileHandle charsheetHandle = files.internal(charsheetUrl.getPath());
            BitmapContext bitmapContext = new BitmapContext(loaded.getCharWidth(),
                    loaded.getCharHeight(),
                    charsheetHandle,
                    new Color(loaded.getControlColor()));
            return bitmapContext;
        }
        throw new FileNotFoundException("There was no file found at this path");
    }

    /**
     * This is currently broken, don't use it.
     */
    public static BitmapContext fromEnum(
            final IncludedTileset includedTileset,final FilesModule files)
            throws FileNotFoundException, URISyntaxException {
        return fromYaml(includedTileset.getPath(), files);
    }
    
    public int getCharPixelWidth() {
        return charPixelWidth;
    }
    
    public int getCharPixelHeight() {
        return charPixelHeight;
    }
    
    public int getCharsheetGridHeight() {
        return CHARSHEET_GRID_HEIGHT;
    }
    
    public FileHandle getCharsheet() {
        return charsheet;
    }
    
    /**
     * Nifty tool for translating a bitmap image into a two-dimensional 
     * array of ImageRepresentations, where the background color of every
     * ImageRepresentation is grabbed from the corresponding pixel of the 
     * bitmap image.
     */
    public ImageRepresentation[][] bmpToImRep(BufferedImage inBMP){
        ImageRepresentation[][] finishedImRepMatrix = 
                new ImageRepresentation[inBMP.getWidth()][inBMP.getHeight()];
        
        for(int i=0;i<inBMP.getWidth();i++){
            for(int j=0;j<inBMP.getHeight(); j++){
                finishedImRepMatrix[i][j] = 
                        new ImageRepresentation(new java.awt.Color(inBMP.getRGB(i,j)),
                                Graphic.FILLED_CELL);
            }
        }
        return finishedImRepMatrix;
    }

    public Color getControlColor() {
        return controlColor;
    }
}
