package tjp.wiji.drawing.frame;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import tjp.wiji.drawing.BitmapContext;
import tjp.wiji.drawing.CellExtents;
import tjp.wiji.drawing.bootstrap.GlModule;
import tjp.wiji.drawing.bootstrap.InputModule;
import tjp.wiji.gui.screen.Screen;
import tjp.wiji.gui.screen.ScreenContext;
import tjp.wiji.representations.ImageRepresentation;
import tjp.wiji.representations.MutableLayer;
import tjp.wiji.representations.Layer;

import java.awt.*;

import static com.google.common.base.Preconditions.checkNotNull;

public class MainFrame extends ApplicationAdapter {

    private final ScreenContext screenContext;
    private final GraphicsPackage graphics;
    private final BitmapContext bitmapContext;
    private final CellExtents cellExtents;
    private Texture spriteSheet;

    private long startTime = 0;
    private long timeSinceLastRender = 0;

    /**
     *
     * @param screenContext
     * @param bitmapContext
     * @param cellExtents
     * @return
     */
    public static MainFrame make(final ScreenContext screenContext,
                                 final BitmapContext bitmapContext,
                                 final CellExtents cellExtents) {
        return make(
                screenContext,
                new GraphicsPackage(),
                bitmapContext,
                cellExtents);
    }

    public static MainFrame make(final ScreenContext screenContext,
                                 final GraphicsPackage graphics,
                                 final BitmapContext bitmapContext,
                                 final CellExtents cellExtents) {

        final MainFrame mainFrame = new MainFrame(
                screenContext, graphics, bitmapContext,
                cellExtents);
        return mainFrame;
    }

    private MainFrame(final ScreenContext screenContext,
                      final GraphicsPackage graphics,
                      final BitmapContext bitmapContext,
                      final CellExtents cellExtents) {
        this.screenContext = screenContext;
        this.graphics = graphics;
        this.bitmapContext = bitmapContext;
        this.cellExtents = cellExtents;
    }

    /**
     * Be warned, GDX has all sort of magic working behind the scenes. If you have weird issues,
     * it is likely that code needs to move from the constructor to here.
     *
     * Also be warned, I think that a render() call can happen before this create() is called.
     */
    @Override
    public void create() {
        WijiInputProcessor.create(screenContext,new InputModule(Gdx.input));

        // DON'T TRY TO MAKE THESE IMMUTABLE, IT JUST WON'T WORK
        graphics.init();
        //bitmapContext.init();
        spriteSheet = new Texture(bitmapContext.getCharsheet());

        // make sure it is 16x16
        assert(spriteSheet.getHeight() % 16 == 0);
    }

    @Override
    public void dispose() {
        graphics.dispose();
        spriteSheet.dispose();
        System.exit(0);
    }

    @Override
    public void render () {
        timeSinceLastRender = System.nanoTime() - startTime;

        Layer cellsToDraw = getCellsToDraw();

        timeSinceLastRender = 0;
        startTime = System.nanoTime();

        if (this.graphics.isInitialized()) {
            this.graphics.clear();
            this.graphics.begin();
            int pixelWidth = bitmapContext.getCharPixelWidth();
            int pixelHeight = bitmapContext.getCharPixelHeight();
            for (int row = 0; row < cellsToDraw.getHeight(); row++) {
                for (int col = 0; col < cellsToDraw.getWidth(); col++) {
                    drawBatch(row, col, pixelWidth, pixelHeight,
                            cellsToDraw.get(col,row).orElse(ImageRepresentation.ERROR));
                    graphics.flush();
                }
            }
            graphics.end();
        }
    }

    /**
     *
     * @param row
     * @param col
     * @param pixelWidth
     * @param pixelHeight
     * @param currCell
     */
    private void drawBatch(int row, int col, int pixelWidth, int pixelHeight,
    ImageRepresentation currCell) {

        checkNotNull(currCell);
        checkNotNull(currCell.getBackColor());
        checkNotNull(currCell.getForeColor());
        configureShaderForCell(currCell.getBackColor(), currCell.getForeColor());

        int graphicIndex = currCell.getImgChar();
        int charsheetWidth = bitmapContext.getCharsheetGridHeight();
        int charCodeX = graphicIndex % charsheetWidth;
        int charCodeY = graphicIndex / charsheetWidth;

        int x = col * pixelWidth;
        int y = (cellExtents.getHeightInSlots() - row - 1) * pixelHeight;
        this.graphics.draw(
                spriteSheet,
                //coordinates in screen space
                //and of the scaling and rotation origin
                //relative to the screen space coordinates
                x, y,
                //width and height in pixels and in texels
                pixelWidth, pixelHeight,
                //coordinates in texel space
                charCodeX * pixelWidth, charCodeY * pixelHeight
        );
    }

    private Layer getCellsToDraw() {
        Screen currentScreen = screenContext.getCurrentScreen();
        if (currentScreen != null) {
            return currentScreen.render(
                    timeSinceLastRender / 1000,
                    cellExtents.getWidthInSlots(),
                    cellExtents.getHeightInSlots());
        }
        else {
            MutableLayer retVal = new MutableLayer(
                    cellExtents.getWidthInSlots(),
                    cellExtents.getHeightInSlots());

            retVal.fill(new ImageRepresentation(Color.BLACK,
                    tjp.wiji.representations.Graphic.EMPTY_CELL));
            return retVal;
        }
    }

    private void configureShaderForCell(final java.awt.Color backColor, final java.awt.Color foreColor) {
        this.graphics.configureShaderForCell(backColor, foreColor);
    }
}
