package tjp.wiji.drawing.frame;

import com.badlogic.gdx.InputProcessor;
import tjp.wiji.drawing.bootstrap.InputModule;
import tjp.wiji.event.GameEvent;
import tjp.wiji.gui.screen.ScreenContext;

/**
 * Intentionally Package Private.
 */
final class WijiInputProcessor implements InputProcessor {
    private final ScreenContext screenContext;

    public static WijiInputProcessor create(
            final ScreenContext screenContext,
            final InputModule inputModule) {
        WijiInputProcessor processor = new WijiInputProcessor(screenContext);
        inputModule.setInputProcessor(processor);
        return processor;
    }

    private WijiInputProcessor(ScreenContext screenContext) {
        this.screenContext = screenContext;
    }

    @Override
    public final boolean keyDown(int keycode) {
        screenContext.handleEvent(new GameEvent(keycode));
        return false;
    }

    @Override
    public final boolean keyTyped(char character) {
        screenContext.handleEvent(new GameEvent(character));
        return false;
    }

    @Override
    public final boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public final boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    /**
     * Called when the mouse wheel was scrolled. Will not be called on iOS.
     *
     * @param amountX the horizontal scroll amount, negative or positive depending on the direction the wheel was scrolled.
     * @param amountY the vertical scroll amount, negative or positive depending on the direction the wheel was scrolled.
     * @return whether the input was processed.
     */
    @Override
    public boolean scrolled(float amountX, float amountY) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }
}
