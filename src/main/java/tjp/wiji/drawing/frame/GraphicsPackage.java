package tjp.wiji.drawing.frame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.ScreenUtils;
import tjp.wiji.drawing.ShaderContext;
import tjp.wiji.drawing.bootstrap.GlModule;

import java.awt.*;
import java.net.URISyntaxException;

public class GraphicsPackage {
    //private final GlModule graphics;
    private SpriteBatch batch;
    private ShaderContext shaderContext;

    private final static float SCALE_FACTOR_255_TO_0_1 = 255f;

    public GraphicsPackage(final GlModule graphics) {
        //this.graphics = graphics;
    }

    public GraphicsPackage() { }

    void init() {
        this.batch = new SpriteBatch();
        this.shaderContext = createShaderContext();
    }

    boolean isInitialized() {
        return this.batch != null
                && this.shaderContext != null;
                //&& this.graphics != null;
    }

    private static ShaderContext createShaderContext() {
        ShaderContext shaderContext = null;
        try {
            shaderContext = new ShaderContext();
        } catch (URISyntaxException e) {
            System.exit(0);
        }
        return shaderContext;
    }

    void dispose() {
        this.batch.dispose();
        this.shaderContext.getShader().dispose();
    }

    void clear() {
        // Clear the Screen
        ScreenUtils.clear(0f,1f,0f,0f);
//        this.graphics.glClearColor(0f, 1f, 0f, 0f);
//        this.graphics.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    void begin() {
        this.batch.begin();
        this.batch.setShader(shaderContext.getShader());
    }

    void flush() {
        this.batch.flush();
    }

    void end() {
        this.batch.end();
    }

    void draw(Texture spriteSheet, int x, int y, int pixelWidth,
              int pixelHeight, int texelX, int texelY) {
        this.batch.draw(
                spriteSheet,
                //coordinates in screen space
                x, y,
                //coordinates of the scaling and rotation origin
                //relative to the screen space coordinates
                x, y,
                //width and height in pixels
                pixelWidth, pixelHeight,
                //scale of the rectangle around originX/originY
                1,1,
                //the angle of counter clockwise rotation of the
                //rectangle around originX/originY
                0,
                //coordinates in texel space
                texelX, texelY,
                //source width and height in texels
                pixelWidth, pixelHeight,
                //whether to flip the sprite horizontally or vertically
                false,false);
    }


    void configureShaderForCell(final Color backColor, final Color foreColor) {
        shaderContext.getShader().setAttributef(
                "a_backColor",
                backColor.getRed() / SCALE_FACTOR_255_TO_0_1,
                backColor.getGreen() / SCALE_FACTOR_255_TO_0_1,
                backColor.getBlue() / SCALE_FACTOR_255_TO_0_1,
                1);

        shaderContext.getShader().setAttributef(
                "a_frontColor",
                foreColor.getRed() / SCALE_FACTOR_255_TO_0_1,
                foreColor.getGreen() / SCALE_FACTOR_255_TO_0_1,
                foreColor.getBlue() / SCALE_FACTOR_255_TO_0_1,
                1);
    }
}
