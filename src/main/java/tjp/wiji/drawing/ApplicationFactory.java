package tjp.wiji.drawing;

import com.badlogic.gdx.Files;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import tjp.wiji.drawing.bootstrap.AppBootstrapper;
import tjp.wiji.drawing.bootstrap.FilesModule;
import tjp.wiji.drawing.frame.MainFrame;
import tjp.wiji.gui.screen.ScreenContext;

import java.net.URISyntaxException;

/**
 * Creates an Application
 */
public class ApplicationFactory {
    private ApplicationFactory() { }

    private static FilesModule makeBootstrappedFilesModule() {
        final AppBootstrapper appBootstrapper = AppBootstrapper.create();
        return appBootstrapper.getFilesModule();
    }

    public static void runEmptyApp() throws URISyntaxException {
        final FilesModule filesModule = makeBootstrappedFilesModule();
        runApp("Sample Frame Title",
                false,
                "AppIcon.png",
                CellExtents.DEFAULT,
                BitmapContext.create(filesModule),
                ScreenContext.create());
    }

    public static void runApp(final ScreenContext screenContext)
            throws URISyntaxException {
        final FilesModule filesModule = makeBootstrappedFilesModule();
        runApp("Sample Frame Title",
                false,
                "AppIcon.png",
                CellExtents.DEFAULT,
                BitmapContext.create(filesModule),
                screenContext);
    }

    public static void runApp(final String title, final ScreenContext screenContext,
                              final FilesModule filesModule)
            throws URISyntaxException {
        makeBootstrappedFilesModule();
        runApp(title,
                false,
                "AppIcon.png",
                CellExtents.DEFAULT,
                BitmapContext.create(filesModule),
                screenContext);
    }

    public static void runApp(final String title, final CellExtents cellExtents,
                              final ScreenContext screenContext)
            throws URISyntaxException {
        final FilesModule filesModule = makeBootstrappedFilesModule();
        runApp(title,
                false,
                "AppIcon.png",
                cellExtents,
                BitmapContext.create(filesModule),
                screenContext);
    }

    public static void runApp(final String title, final boolean resizable, final String icon,
                              final CellExtents cellExtents, final BitmapContext bitmapContext,
                              final ScreenContext screenContext) {
        runApp(title, resizable, icon, cellExtents,
                bitmapContext, screenContext,
                AppBootstrapper.create());

    }

    public static void runApp(final String title, final boolean resizable, final String icon,
                              final CellExtents cellExtents, final BitmapContext bitmapContext,
                              final ScreenContext screenContext,
                              final AppBootstrapper appBootstrapper) {
        Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
        config.setTitle(title);
        config.setWindowedMode(
                cellExtents.getWidthInSlots() * bitmapContext.getCharPixelWidth(),
                cellExtents.getHeightInSlots() * bitmapContext.getCharPixelHeight());
        config.setResizable(resizable);
        config.setWindowIcon(Files.FileType.Internal, icon);

        final MainFrame frame = MainFrame.make(
                screenContext,
                bitmapContext,
                cellExtents);

        new Lwjgl3Application(frame, config);

        // frame.attachInputListener();
    }
}
