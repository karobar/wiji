package tjp.wiji.drawing;

import java.awt.*;

public class SupplementalColors {
    protected static final Color DEFAULT_INACTIVE_COLOR = Color.WHITE;
    protected static final Color DEFAULT_ACTIVE_COLOR = Color.YELLOW;

    public static final Color BROWN = new Color(170, 85, 0);
    public static final Color DARK_GRAY = new Color(42,42,42);
    public static final Color LIGHT_BLUE = new Color(85,85,255);
    public static final Color LIGHT_GREEN = new Color(85,255,85);
    public static final Color LIGHT_CYAN = new Color(85,255,255);
    public static final Color LIGHT_RED = new Color(255,85,85);
    public static final Color LIGHT_MAGENTA = new Color(255,85,255);
}
