package tjp.wiji.drawing;

import com.google.errorprone.annotations.Immutable;

@Immutable
public class CellExtents {
    private int widthInSlots, heightInSlots;

    public static final CellExtents DEFAULT = new CellExtents(80,25);

    public CellExtents(int widthInSlots, int heightInSlots) {
        this.widthInSlots = widthInSlots;
        this.heightInSlots = heightInSlots;
    }

    public int getWidthInSlots() {
        return widthInSlots;
    }

    public int getHeightInSlots() {
        return heightInSlots;
    }
}
