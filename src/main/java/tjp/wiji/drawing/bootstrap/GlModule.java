package tjp.wiji.drawing.bootstrap;

import com.badlogic.gdx.graphics.GL20;

/**
 * This will be initiated.
 */
public class GlModule {
    private final GL20 delegate;
    /**
     * Package-private by design, only construct from AppBootstrapper.
     */
    GlModule (final GL20 gl20) {
        this.delegate = gl20;
    }

    public void glClearColor(float v, float v1, float v2, float v3) {
        this.delegate.glClearColor(v,v1,v2,v3);
    }

    public void glClear(final int mask) {
        this.delegate.glClear(mask);
    }
}

