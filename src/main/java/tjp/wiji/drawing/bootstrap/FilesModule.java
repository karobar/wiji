package tjp.wiji.drawing.bootstrap;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.files.FileHandle;

/**
 * This will be initiated.
 */
public class FilesModule {
    private final Files delegate;
    /**
     * Package-private by design, only construct from AppBootstrapper.
     */
    FilesModule(Files files) {
        this.delegate = files;
    }

    public FileHandle internal (final String path) {
        return this.delegate.internal(path);
    }
}
