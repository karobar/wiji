package tjp.wiji.drawing.bootstrap;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import static com.google.common.base.Preconditions.checkNotNull;

public class AppBootstrapper {
    private final HeadlessApplication delegate;

    public static AppBootstrapper create() {
        ApplicationListener applicationListener = new ApplicationListener() {
            @Override
            public void create() { }

            @Override
            public void resize(int width, int height) { }

            @Override
            public void render() { }

            @Override
            public void pause() { }

            @Override
            public void resume() { }

            @Override
            public void dispose() { }
        };
        final HeadlessApplicationConfiguration conf = new HeadlessApplicationConfiguration();
        final HeadlessApplication headlessApplication =
                new HeadlessApplication(applicationListener, conf);
        //Gdx.gl20 = Mockito.mock(GL20.class);
        Gdx.gl = Gdx.gl20;
        return new AppBootstrapper(headlessApplication);
    }

    private AppBootstrapper (final HeadlessApplication headlessApplication) {
        this.delegate = headlessApplication;
    }

    public FilesModule getFilesModule() {
        checkNotNull(Gdx.files);
        return new FilesModule(Gdx.files);
    }
//
//    public GlModule getGlModule() {
//        checkNotNull(Gdx.gl);
//        return new GlModule(Gdx.gl);
//    }

    public InputModule getInputModule() {
        checkNotNull(Gdx.input);
        return new InputModule(Gdx.input);
    }
}
