package tjp.wiji.drawing.bootstrap;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;

public class InputModule {
    private final Input delegate;
    /**
     * Package-private by design, only construct from AppBootstrapper.
     */
    public InputModule (final Input input) {
        this.delegate = input;
    }

    public void setInputProcessor(final InputProcessor processor) {
        this.delegate.setInputProcessor(processor);
    }
}
