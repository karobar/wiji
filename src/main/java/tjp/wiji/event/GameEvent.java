package tjp.wiji.event;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author      Travis Pressler (travisp471@gmail.com)
 * @version     %I%, %G%
 */
public class GameEvent {
    private final Key key;

    public GameEvent(int keycode) {
        this.key = new Key(keycode);
    }
    
    public GameEvent(char keycode) {
        this.key = new Key(keycode);
    }

    /**
     * TODO: remove
     * @return
     */
    public int getIntCode() {
        return this.key.getEscapeCode();
    }

    /**
     * TODO: remove
     * @return
     */
    public char getCharCode() {
        return (char) this.key.getEscapeCode();
    }

    public Key getKey() {
        return key;
    }
    

    

    

}
