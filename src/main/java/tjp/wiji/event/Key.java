package tjp.wiji.event;

import com.badlogic.gdx.Input;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Key {
    public static Key ENTER = new Key(Input.Keys.ENTER);
    public static Key ESCAPE = new Key(Input.Keys.ESCAPE);
    public static Key UP = new Key(Input.Keys.UP);
    public static Key DOWN = new Key(Input.Keys.DOWN);
    public static Key LEFT = new Key(Input.Keys.LEFT);
    public static Key RIGHT = new Key(Input.Keys.RIGHT);

    public static Key X = new Key(Input.Keys.X);

    private static final Set<Character> NORMAL_CHARS =
            new HashSet<Character>(Arrays.asList('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                    'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'));

    private static final Set<Character> NUMBERS = new HashSet<Character>(
            Arrays.asList('0', '1', '2', '3', '4', '5', '6', '7', '8', '9'));

    public boolean isNumber() {
        return isCharOfType((char) this.escapeCode, NUMBERS);
    }

    public boolean isNormalTypingKey() {
        return isCharOfType((char) this.escapeCode, NORMAL_CHARS);
    }

    private boolean isCharOfType(char searchChar, Set<Character> set) {
        if ('\u0000' == searchChar) {
            return false;
        }

        boolean found = false;
        // think the Character wrapper causes problems with ==
        for (Character ch : set) {
            if (ch.equals(searchChar)) {
                found = true;
            }
        }
        return found;
    }

    private final int escapeCode;

    Key(int escapeCode) {
        this.escapeCode = escapeCode;
    }

    /**
     * TODO: remove
     * @return
     */
    int getEscapeCode() {
        return escapeCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Key key = (Key) o;
        return escapeCode == key.escapeCode;
    }

    @Override
    public int hashCode() {
        return Objects.hash(escapeCode);
    }
}
