package tjp.wiji.representations;

import com.google.common.collect.ImmutableBiMap;
import com.google.errorprone.annotations.Immutable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Immutable
public enum Graphic {
    EMPTY_CELL(0),
    SMILEY_BLACK(1),
    SMILEY_WHITE(2),
    HEART(3),
    DIAMOND(4),
    CLUB(5),
    SPADE(6),
    BULLET(7),
    BULLET_INVERTED(8),
    DONUT(9),
    DONUT_INVERTED(10),
    GENDER_MALE_SYMBOL(11),
    GENDER_FEMALE_SYMBOL(12),
    MUSIC_EIGHTH_NOTE(13),
    MUSIC_BEAMED_SIXTEENTH_NOTES(14),
    SOLAR_SYMBOL(15),
    TRIANGLE_RIGHT(16),
    TRIANGLE_LEFT(17),
    ARROW_UP_DOWN(18),
    DOUBLE_EXCLAMATION_MARK(19),
    PARAGRAPH_SYMBOL(20),
    SECTION_SYMBOL(21),
    UNDERSCORE_THICK(22),
    ARROW_UP_DOWN_UNDERLINE(23),
    ARROW_UP(24),
    ARROW_DOWN(25),
    ARROW_RIGHT(26),
    ARROW_LEFT(27),
    RIGHT_ANGLE(28),
    ARROW_LEFT_RIGHT(29),
    TRIANGLE_UP(30),
    TRIANGLE_DOWN(31),
    EMPTY_CELL_2(32),
    EXCLAMATION_POINT(33),
    QUOTATION_MARK(34),
    OCTOTHORPE(35),
    DOLLAR_SIGN(36),
    PERCENTAGE(37),
    AMPERSAND(38),
    QUOTATION_MARK_SINGLE(39),
    PARENTHESIS_LEFT(40),
    PARENTHESIS_RIGHT(41),
    ASTERISK(42),
    PLUS(43),
    COMMA(44),
    HYPHEN_MINUS(45),
    PERIOD(46),
    FORWARD_SLASH(47),
    ZERO(48),
    ONE(49),
    TWO(50),
    THREE(51),
    FOUR(52),
    FIVE(53),
    SIX(54),
    SEVEN(55),
    EIGHT(56),
    NINE(57),
    COLON(58),
    SEMICOLON(59),
    LESS_THAN(60),
    EQUALS(61),
    GREATER_THAN(62),
    QUESTION_MARK(63),
    AT_SYMBOL(64),
    UPPERCASE_A(65),
    UPPERCASE_B(66),
    UPPERCASE_C(67),
    UPPERCASE_D(68),
    UPPERCASE_E(69),
    UPPERCASE_F(70),
    UPPERCASE_G(71),
    UPPERCASE_H(72),
    UPPERCASE_I(73),
    UPPERCASE_J(74),
    UPPERCASE_K(75),
    UPPERCASE_L(76),
    UPPERCASE_M(77),
    UPPERCASE_N(78),
    UPPERCASE_O(79),
    UPPERCASE_P(80),
    UPPERCASE_Q(81),
    UPPERCASE_R(82),
    UPPERCASE_S(83),
    UPPERCASE_T(84),
    UPPERCASE_U(85),
    UPPERCASE_V(86),
    UPPERCASE_W(87),
    UPPERCASE_X(88),
    UPPERCASE_Y(89),
    UPPERCASE_Z(90),
    LEFT_SQUARE_BRACKET(91),
    BACKSLASH(92),
    RIGHT_SQUARE_BRACKET(93),
    CARAT(94),
    UNDERLINE(95),
    BACKTICK(96),
    LOWERCASE_A(97),
    LOWERCASE_B(98),
    LOWERCASE_C(99),
    LOWERCASE_D(100),
    LOWERCASE_E(101),
    LOWERCASE_F(102),
    LOWERCASE_G(103),
    LOWERCASE_H(104),
    LOWERCASE_I(105),
    LOWERCASE_J(106),
    LOWERCASE_L(107),
    LOWERCASE_K(108),
    LOWERCASE_M(109),
    LOWERCASE_N(110),
    LOWERCASE_O(111),
    LOWERCASE_P(112),
    LOWERCASE_Q(113),
    LOWERCASE_R(114),
    LOWERCASE_S(115),
    LOWERCASE_T(116),
    LOWERCASE_U(117),
    LOWERCASE_V(118),
    LOWERCASE_W(119),
    LOWERCASE_X(120),
    LOWERCASE_Y(121),
    LOWERCASE_Z(122),
    LIGHT_MIST(176),
    MIST(177),
    HEAVY_MIST(178),
    FILLED_CELL(219),
    CENTERED_DOT(250);

    private final int rawImageChar;

    private static final Map<Character, Graphic> charToGraphic =
            ImmutableBiMap.<Character, Graphic>builder()
                    .put(' ',EMPTY_CELL)
                    .put('!',EXCLAMATION_POINT)
                    .put('"',QUOTATION_MARK)
                    .put('#',OCTOTHORPE)
                    .put('$',DOLLAR_SIGN)
                    .put('%',PERCENTAGE)
                    .put('&',AMPERSAND)
                    .put('\'',QUOTATION_MARK_SINGLE)
                    .put('(',PARENTHESIS_LEFT)
                    .put(')',PARENTHESIS_RIGHT)
                    .put('*',ASTERISK)
                    .put('+',PLUS)
                    .put(',',COMMA)
                    .put('-',HYPHEN_MINUS)
                    .put('.',PERIOD)
                    .put('/',FORWARD_SLASH)
                    .put('0',ZERO)
                    .put('1',ONE)
                    .put('2',TWO)
                    .put('3',THREE)
                    .put('4',FOUR)
                    .put('5',FIVE)
                    .put('6',SIX)
                    .put('7',SEVEN)
                    .put('8',EIGHT)
                    .put('9',NINE)
                    .put(':',COLON)
                    .put(';',SEMICOLON)
                    .put('<',LESS_THAN)
                    .put('=',EQUALS)
                    .put('>',GREATER_THAN)
                    .put('?',QUESTION_MARK)
                    .put('@',AT_SYMBOL)
                    .put('A',UPPERCASE_A)
                    .put('B',UPPERCASE_B)
                    .put('C',UPPERCASE_C)
                    .put('D',UPPERCASE_D)
                    .put('E',UPPERCASE_E)
                    .put('F',UPPERCASE_F)
                    .put('G',UPPERCASE_G)
                    .put('H',UPPERCASE_H)
                    .put('I',UPPERCASE_I)
                    .put('J',UPPERCASE_J)
                    .put('K',UPPERCASE_K)
                    .put('L',UPPERCASE_L)
                    .put('M',UPPERCASE_M)
                    .put('N',UPPERCASE_N)
                    .put('O',UPPERCASE_O)
                    .put('P',UPPERCASE_P)
                    .put('Q',UPPERCASE_Q)
                    .put('R',UPPERCASE_R)
                    .put('S',UPPERCASE_S)
                    .put('T',UPPERCASE_T)
                    .put('U',UPPERCASE_U)
                    .put('V',UPPERCASE_V)
                    .put('W',UPPERCASE_W)
                    .put('X',UPPERCASE_X)
                    .put('Y',UPPERCASE_Y)
                    .put('Z',UPPERCASE_Z)
                    .put('[',LEFT_SQUARE_BRACKET)
                    .put('\\',BACKSLASH)
                    .put(']',RIGHT_SQUARE_BRACKET)
                    .put('^',CARAT)
                    .put('_',UNDERLINE)
                    .put('`',BACKTICK)
                    .put('a',LOWERCASE_A)
                    .put('b',LOWERCASE_B)
                    .put('c',LOWERCASE_C)
                    .put('d',LOWERCASE_D)
                    .put('e',LOWERCASE_E)
                    .put('f',LOWERCASE_F)
                    .put('g',LOWERCASE_G)
                    .put('h',LOWERCASE_H)
                    .put('i',LOWERCASE_I)
                    .put('j',LOWERCASE_J)
                    .put('k',LOWERCASE_L)
                    .put('l',LOWERCASE_K)
                    .put('m',LOWERCASE_M)
                    .put('n',LOWERCASE_N)
                    .put('o',LOWERCASE_O)
                    .put('p',LOWERCASE_P)
                    .put('q',LOWERCASE_Q)
                    .put('r',LOWERCASE_R)
                    .put('s',LOWERCASE_S)
                    .put('t',LOWERCASE_T)
                    .put('u',LOWERCASE_U)
                    .put('v',LOWERCASE_V)
                    .put('w',LOWERCASE_W)
                    .put('x',LOWERCASE_X)
                    .put('y',LOWERCASE_Y)
                    .put('z',LOWERCASE_Z)
                    .build();

    
    Graphic(int rawImageChar) {
        this.rawImageChar = rawImageChar;
    }
    
    public int getRawImageChar() {
        return rawImageChar;
    }

    public static List<Graphic> fromString(String string) {
        List<Graphic> graphics = new ArrayList<>();
        for (int i = 0; i < string.length(); i++) {
            graphics.add(charToGraphic.get(string.charAt(i)));
        }
        return graphics;
    }
}
