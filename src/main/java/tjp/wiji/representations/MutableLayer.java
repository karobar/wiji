package tjp.wiji.representations;


import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Deprecated
public class MutableLayer implements Layer {
    private final ImageRepresentation[][] field;

    @Deprecated
    public MutableLayer(int width, int height) {
        field = new ImageRepresentation[width][height];
    }

    /**
     * Init with row of text
     * @param reps
     */
    @Deprecated
    public static MutableLayer row(final List<ImageRepresentation> reps) {
        final MutableLayer field = new MutableLayer(reps.size(),1);

        for (int i = 0; i < reps.size(); i++) {
            field.put(reps.get(i), i, 0);
        }

        return field;
    }

    @Override
    public final int getHeight() {
        return field[0].length;
    }

    @Override
    public final int getWidth() {
        return field.length;
    }

    @Deprecated
    public final void put(ImageRepresentation imageRepresentation, int x, int y) {
        this.field[x][y] = imageRepresentation;
    }

    @Deprecated
    public final Optional<ImageRepresentation> get(int x, int y) {
        try {
            return Optional.of(this.field[x][y]);
        } catch (ArrayIndexOutOfBoundsException e) {
            return Optional.empty();
        }
    }

    @Deprecated
    public final void fill(ImageRepresentation representation) {
        for(ImageRepresentation[] col : this.field) {
            Arrays.fill(col, representation);
        }
    }

    /**
     * Overlay this RepresentationField onto another representation field
     * @param layer
     */
    @Deprecated
    public final Layer overlay(Layer layer) {
        for(int x = 0; x < layer.getWidth(); x++) {
            for(int y = 0; y < layer.getHeight(); y++) {
                Optional<ImageRepresentation> overlayCell = layer.get(x, y);
                if (overlayCell.isPresent()) {
                    this.put(overlayCell.get(), x, y);
                }
            }
        }
        return this;
    }

    /**
     * Overlay this over another representation field.
     *
     * @param underlay
     * @param xOffset
     * @param yOffset
     * @return
     */
    @Override
    public Layer overlay(Layer underlay, int xOffset, int yOffset) {
        throw new UnsupportedOperationException("haven't implemented this yet");
    }
}
