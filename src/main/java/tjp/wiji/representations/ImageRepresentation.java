package tjp.wiji.representations;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.errorprone.annotations.Immutable;


import java.awt.*;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 *  An ImageRepresentation is the visual representation which occupies one of 
 *  the game window's cells. In other words, an ImageRepresentation represents 
 *  all sprites and other graphics.
 * 
 * @author      Travis Pressler (travisp471@gmail.com)
 * @version     %I%, %G%
 */
@Immutable
public class ImageRepresentation {
    public static final ImageRepresentation ERROR = new ImageRepresentation(
            Color.MAGENTA, Color.GREEN, Graphic.QUESTION_MARK);
    public static final ImageRepresentation ALL_BLACK = new ImageRepresentation(
            Color.BLACK, Color.BLACK, Graphic.EMPTY_CELL);

    private final Color foreColor;
    private final int imgChar;
    private final Color backColor;

    /**
     * Creates a white ImageRep.
     */
    public ImageRepresentation(final int imgChar) {
        this(Color.WHITE, Color.BLACK, imgChar);
    }

    /**
     * An ImageRepresentation where only the foreColor is explicitly 
     * defined, the backColor is likely to be implied by the background 
     * color of the floor below it.
     */
    public ImageRepresentation(final int imgChar, final Color foreColor) {
        this(foreColor, Color.BLACK, imgChar);
    }
    
    /**
     *  An ImageRepresentation where the foreColor and backColor are 
     *  explicitly defined.
     */
    public ImageRepresentation(final Color foreColor, final Color backColor, int imgChar) {
        this.foreColor = checkNotNull(foreColor);
        this.backColor = checkNotNull(backColor);
        this.imgChar = imgChar;
    }

    /**
     * Creates a white ImageRep.
     */
    public ImageRepresentation(final Graphic sprite) {
        this(Color.WHITE, Color.BLACK, sprite);
    }

    /**
     * An ImageRepresentation where only the foreColor and sprite is explicitly
     * defined, the backColor is likely to be implied by the background
     * color of the floor below it.
     */
    public ImageRepresentation(final Color foreColor, final Graphic sprite) {
        this(foreColor, Color.BLACK, sprite);
    }

    /**
     * An ImageRepresentation where only the foreColor is explicitly
     * defined, the backColor is likely to be implied by the background
     * color of the floor below it.
     */
    public ImageRepresentation(final Color foreColor) {
        this(foreColor, Color.BLACK, Graphic.FILLED_CELL);
    }

    /**
     *  An ImageRepresentation where the foreColor and backColor are
     *  explicitly defined.
     */
    public ImageRepresentation(Color foreColor, Color backColor, final Graphic sprite) {
        this.foreColor = checkNotNull(foreColor);
        this.backColor = checkNotNull(backColor);
        this.imgChar = sprite.getRawImageChar();
    }

    public Color getForeColor() {
        return foreColor;
    }

    public Color getBackColor() {
        return backColor;
    }
    
    public int getImgChar() {
        return this.imgChar;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass()) { return false; }
        ImageRepresentation other = (ImageRepresentation) obj;
        return Objects.equal(this.backColor, other.backColor)
                && Objects.equal(this.foreColor, other.foreColor)
                && Objects.equal(this.imgChar, other.imgChar);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(
            backColor, foreColor, imgChar
        );
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("foreColor", foreColor)
                .add("backColor", backColor)
                .add("imgChar", imgChar)
                .toString();

    }
}