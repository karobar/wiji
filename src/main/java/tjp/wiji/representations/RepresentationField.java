package tjp.wiji.representations;

import java.util.Optional;

public interface RepresentationField {
    Optional<ImageRepresentation> get(int x, int y);
}
