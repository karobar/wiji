package tjp.wiji.representations;

public interface Layer extends RepresentationField {
    /**
     * Y Coordinates (up-down)
     * @return
     */
    int getHeight();

    /**
     * X Coordinates (left-right)
     * @return
     */
    int getWidth();

    /**
     * Overlay this over another representation field.
     * @param underlay
     * @return
     */
    Layer overlay(final Layer underlay);

    /**
     * Overlay this over another representation field.
     * @param underlay
     * @param xOffset
     * @param yOffset
     * @return
     */
    Layer overlay(final Layer underlay, int xOffset, int yOffset);
}
