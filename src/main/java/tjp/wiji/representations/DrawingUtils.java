package tjp.wiji.representations;


import java.awt.*;
import java.awt.image.BufferedImage;

public class DrawingUtils {
    /**
     * Nifty tool for translating a bitmap image into a two-dimensional 
     * array of ImageRepresentations, where the background color of every
     * ImageRepresentation is grabbed from the corresponding pixel of the 
     * bitmap image.
     */
    public static ImageRepresentation[][] bmpToImRep(int width, int height, BufferedImage inBMP){
        ImageRepresentation[][] finishedImRepMatrix = 
                new ImageRepresentation[inBMP.getWidth()][inBMP.getHeight()];
        if((inBMP.getWidth() > width)|| (inBMP.getHeight() > height)){
            System.out.println("Yo, something's wrong with the title screen size.");
        }
        for(int i=0;i<inBMP.getWidth();i++){
            for(int j=0;j<inBMP.getHeight(); j++){
                finishedImRepMatrix[i][j] = 
                        new ImageRepresentation(
                                Graphic.FILLED_CELL.getRawImageChar(),
                                new Color(inBMP.getRGB(i,j)));
            }
        }
        return finishedImRepMatrix;
    }
}
