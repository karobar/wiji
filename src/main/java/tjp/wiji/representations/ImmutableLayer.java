package tjp.wiji.representations;

import com.google.common.collect.ImmutableList;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;

public class ImmutableLayer implements Layer {
    /**
     * This is row-major (the outermost list refers to rows)
     */
    private final ImmutableList<ImmutableList <ImageRepresentation>> field;

    /**
     * This is row-major (the outermost list refers to rows)
     */
    private ImmutableLayer(final ImmutableList<ImmutableList<ImageRepresentation>> field) {
        this.field = field;
    }

    /**
     * This is row-major (the outermost list refers to rows)
     */
    public static ImmutableLayer create(final ImmutableList<ImmutableList<ImageRepresentation>> field) {
        Set<Integer> foundWidths = new HashSet<>();
        for (ImmutableList<ImageRepresentation> row : field) {
            foundWidths.add(row.size());
        }
        if (foundWidths.size() > 1) {
            throw new IllegalArgumentException("For now, this class only supports uniform widths");
        }
        return new ImmutableLayer(field);
    }

    /**
     * Init with row of text
     * @param reps
     */
    public static ImmutableLayer row(final List<ImageRepresentation> reps) {
        return new ImmutableLayer(ImmutableList.of(ImmutableList.copyOf(reps)));
    }

    /**
     * For 1x1s
     * @param rep
     * @return
     */
    public static ImmutableLayer create(final ImageRepresentation rep) {
        return create(ImmutableList.of(ImmutableList.of(rep)));
    }

    public static ImmutableLayer fromTransparency(final BufferedImage transparency) {
        final List<ImmutableList<ImageRepresentation>> matrix = new ArrayList<>();
        for (int y = 0; y < transparency.getHeight(); y++) {
            final List<ImageRepresentation> row = new ArrayList<>();
            for (int x = 0; x < transparency.getWidth(); x++) {
                Color color = new Color(transparency.getRGB(x, y), true);
                row.add(new ImageRepresentation(color,color, Graphic.FILLED_CELL));
            }
            matrix.add(ImmutableList.copyOf(row));
        }
        return new ImmutableLayer(ImmutableList.copyOf(matrix));
    }

    /**
     * X Coordinates
     *
     * @return
     */
    @Override
    public int getHeight() {
        return field.size();
    }

    /**
     * Y Coordinates (up-down)
     *
     * @return
     */
    @Override
    public int getWidth() {
        int max = 0;
        for (ImmutableList<ImageRepresentation> row : field) {
            int rowSize = row.size();
            if (rowSize > max) {
                max = rowSize;
            }
        }
        return max;
    }

    /**
     * Overlay this over another representation field. Underlay must be larger.
     *
     * @param underlay
     * @return
     */
    @Override
    public Layer overlay(final Layer underlay) {
        return overlay(underlay, 0, 0);
    }

    /**
     * Overlay this over another representation field. Underlay must be larger.
     *
     * @param underlay
     * @param xOffset x coordinate offset in underlay's space
     * @param yOffset y coordinate offset in underlay's space
     * @return
     */
    @Override
    public Layer overlay(final Layer underlay, int xOffset, int yOffset) {
        if (underlay.getWidth() < getWidth() || underlay.getHeight() < getHeight()) {
            throw new IllegalArgumentException("underlay must be larger than what you're overlaying");
        }

        final List<ImmutableList<ImageRepresentation>> newList = new ArrayList<>();
        for(int y = 0; y < underlay.getHeight(); y++) {
            List<ImageRepresentation> newRow = new ArrayList<>();

            for(int x = 0; x < underlay.getWidth(); x++) {
                Optional<ImageRepresentation> underlayCell = underlay.get(x, y);
                if (underlayCell.isPresent()) {
                    Optional<ImageRepresentation>  overlayCell = this.get(x-xOffset, y-yOffset);
                    if (overlayCell.isPresent()) {
                        newRow.add(overlayCell.get());
                    } else {
                        newRow.add(underlayCell.get());
                    }
                }
            }
            newList.add(ImmutableList.copyOf(newRow));
        }
        return new ImmutableLayer(ImmutableList.copyOf(newList));
    }

    @Override
    public Optional<ImageRepresentation> get(int x, int y) {
        try {
            return Optional.of(field.get(y).get(x));
        } catch(IndexOutOfBoundsException e) {
            return Optional.empty();
        }
    }
}
