package tjp.wiji.gui;

public interface Cyclable {
    void cycleUp();
    void cycleDown();
}
