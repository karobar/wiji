package tjp.wiji.gui.gui_element;

import tjp.wiji.drawing.BitmapContext;
import tjp.wiji.drawing.CellDimensions;
import java.awt.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Basically a Collection<GUIText> but with extra handling of current item, backed by a List.
 * @author      Travis Pressler (travisp471@gmail.com)
 * @version     %I%, %G%
 */
public class ScreenTextList extends ParentTextCollection {
    public static class Builder {
        private BitmapContext bitmapContext;

        private boolean centered = false;
        //private CellDimensions dims;
        private Color inactiveColor, activeColor;
        private String initialItem;
        private int x, y;

        public Builder activeColor(Color activeColor) {
            this.activeColor = activeColor;
            return this;
        }

        public Builder bitmapContext(BitmapContext bitmapContext) {
            this.bitmapContext = bitmapContext;
            return this;
        }

        /**
         * TODO: add all sorts of checks to make sure required combinations
         * are provided in full.
         * @return
         */
        public ScreenTextList build() {
            Color outActiveColor = this.activeColor == null ?
                        GuiElement.DEFAULT_ACTIVE_COLOR : this.activeColor;

            Color outInactiveColor = this.inactiveColor == null ?
                    GuiElement.DEFAULT_INACTIVE_COLOR : this.inactiveColor;

            return new ScreenTextList(this.bitmapContext, outActiveColor,
                    outInactiveColor, this.x, this.y);
        }

        public Builder centered() {
            this.centered = true;
            return this;
        }

        public Builder color(Color color) {
            this.inactiveColor = color;
            this.activeColor = color;
            return this;
        }

        public Builder inactiveColor(Color inactiveColor) {
            this.inactiveColor = inactiveColor;
            return this;
        }

        public Builder initialItem(String initialItem) {
            this.initialItem = initialItem;
            return this;
        }

        public Builder x(int x) {
            this.x = x;
            return this;
        }

        public Builder y(int y) {
            this.y = y;
            return this;
        }
    }

    protected BitmapContext bitmapContext;

    protected boolean centered = false;

    private final List<GUItext> textList = new ArrayList<GUItext>();

    protected int screenX, screenY;

    private GuiElement tertiaryElement;

    /**
     * Basic Constructor for TextCollection for text which does not change
     * colors and contains one text item.
     * @param
     * @param color color of the text
     * @param x position of upper-left corner of the list (assuming
     *        position is not explicitly assigned with GUIText.specX)
     * @param y position of upper-left corner of the list (assuming
     *        position is not explicitly assigned with GUIText.specY)
     */
    public ScreenTextList(BitmapContext bitmapContext, CellDimensions dimension,
                          Color color, String initialItem, int x, int y) {
        super(bitmapContext, color, color, x, y, false);
        this.add(new GUItext(initialItem));
    }

    /**
     * Augmented Base constructor for text which changes color and which does not have
     * coordinates, uses the default.
     */
    public ScreenTextList(final BitmapContext bitmapContext){
        this(bitmapContext,
                DEFAULT_INACTIVE_COLOR, DEFAULT_ACTIVE_COLOR,
                0, 0);
    }

    /**
     * Augmented Base constructor for text which changes color and which does not have
     * coordinates.
     * @param inactiveColor color of the text
     * @param activeColor color of the text when active
   */
   public ScreenTextList(final BitmapContext bitmapContext,
                         Color inactiveColor, Color activeColor){
       super(bitmapContext, activeColor, inactiveColor,
               0, 0, false);
   }

    /**
      * Augmented Base constructor for text which changes color.
      * @param inactiveColor color of the text
      * @param activeColor color of the text when active
      * @param x position of upper-left corner of the list (assuming
      *        position is not explicitly assigned with GUIText.specX)
      * @param y position of upper-left corner of the list (assuming
      *        position is not explicitly assigned with GUIText.specY)
    */
    public ScreenTextList(final BitmapContext bitmapContext,
                          Color inactiveColor, Color activeColor,
                          int x, int y){
        super(bitmapContext, activeColor, inactiveColor,
                x, y, false);
    }

    /**
      * Basic Constructor for TextCollection for text which does not change
      * colors.
      * @param color color of the text
      * @param x position of upper-left corner of the list (assuming
      *        position is not explicitly assigned with GUIText.specX)
      * @param y position of upper-left corner of the list (assuming
      *        position is not explicitly assigned with GUIText.specY)
    */
    public ScreenTextList(BitmapContext bitmapContext, Color color, int x, int y) {
        this(bitmapContext, color, color, x, y);
    }

    /**
     * Basic Constructor for TextCollection for text which does not change
     * colors.
     * @param x position of upper-left corner of the list (assuming
     *        position is not explicitly assigned with GUIText.specX)
     * @param y position of upper-left corner of the list (assuming
     *        position is not explicitly assigned with GUIText.specY)
     */
    public ScreenTextList(BitmapContext bitmapContext, int x, int y) {
        this(bitmapContext, DEFAULT_ACTIVE_COLOR, x, y);
    }

     /**
     * Basic Constructor for TextCollection for text which does not change
     * colors and contains one text item.
     * @param
     * @param color color of the text
     * @param x position of upper-left corner of the list (assuming
     *        position is not explicitly assigned with GUIText.specX)
     * @param y position of upper-left corner of the list (assuming
     *        position is not explicitly assigned with GUIText.specY)
   */
   public ScreenTextList(BitmapContext bitmapContext,
                         Color color, String initialItem, int x, int y) {
       this(bitmapContext, color, color, x, y);
       this.add(new GUItext(initialItem));
   }

    public boolean add(GUItext text) {
        return textList.add(text);
    }

    public GUItext getCurrentChoice() {
        return textList.get(currentChoiceIndex);
    }

    Color getInactiveColor() {
        return inactiveColor;
    }

    Color getActiveColor() {
        return activeColor;
    }

    public void cycleUp() {
        System.out.println("Cycling up!");
        if(currentChoiceIndex > 0) {
            currentChoiceIndex = currentChoiceIndex - 1;
        }
        else if (currentChoiceIndex == 0){
            currentChoiceIndex = textList.size()-1;
        }
        else {
            System.out.println("yo, cycleActiveUp in ChoiceList is bein' wierd");
        }
        if (textList.get(currentChoiceIndex).isAncillary()) {
            cycleUp();
        }
        cycleUpHook();
    }

    public void cycleDown() {
        System.out.println("Cycling down!");
        currentChoiceIndex = (currentChoiceIndex + 1) % textList.size();
        if (textList.get(currentChoiceIndex).isAncillary()) {
            cycleDown();
        }
        cycleDownHook();
    }

    @Override
    public int getHeight() { return textList.size(); }

    @Override
    public GUItext get(int index) {
        // System.out.println("Getting element at index: " + index + " out of size: " + textList.size() );
        return textList.get(index);
    }

    public void setTertiaryElement(GuiElement tertiaryElement) {
        this.tertiaryElement = tertiaryElement;
    }

    public int getScreenX() {
        return screenX;
    }

    public int getScreenY() {
        return screenY;
    }

    protected int calculateOffset() {
        return 0;
    }

    /**
     * This is the number of elements in this list which will show up on the screen.
     * @return
     */
    protected int getListHeight() { return getHeight(); }

    public boolean isAncillary() {
        return false;
    }

    public boolean isCentered() {
        return centered;
    }

    void setScreenX(int inX) {
        screenX = inX;
    }

    void setScreenY(int inY) {
        screenY = inY;
    }

    protected void cycleUpHook() {}

    protected void cycleDownHook() {}


}
