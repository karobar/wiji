package tjp.wiji.gui.gui_element;

import tjp.wiji.drawing.BitmapContext;
import tjp.wiji.representations.ImageRepresentation;
import tjp.wiji.representations.LetterRepresentation;
import tjp.wiji.representations.MutableLayer;

import java.awt.*;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A GUIText object is basically just a string with additional functionality for
 * distinguishing between mainstream textString items and ancillary textString 
 * items (usually ancillary textString is used for information related to the 
 * ChoiceList to which the GUIText is linked).
 * 
 * @author      Travis Pressler (travisp471@gmail.com)
 * @version     %I%, %G%
 */
@Deprecated
public class GUItext extends GuiElement implements Comparable<GUItext> {
    public final static Color DEFAULT_ANCILLARY_TEXT_COLOR = Color.GRAY;
    /** 
     * If a GUIText element is a BGthief, it will steal the color from the
     * element underneath. 
     */
    public boolean BGthief = false;
    private boolean isAncillary;
    
    //int[] textCodes;

    private String text;

    /**
     * Basic constructor for a textString item.
     * @param text the text to be displayed
     */
    public GUItext(String text) {
        this.text = text;
        //textCodes = Translator.translate(text);
    }
    
    /**
     * Basic constructor for an ancillary text item.
     * @param text the text to be displayed
     */
    public GUItext(String text, boolean isAncillary) {
        this.text = text;
        this.isAncillary = true;
    }

    /**
     * Basic constructor for a normal text item with a color.
     * @param text the text string to be displayed
     * @param color the color to display the text in
     */
    public GUItext(final String text, final Color color) {
        this(text, color, false);
    }

    /**
     * Basic constructor for an ancillary text item with a color.
     * @param text the text string to be displayed
     * @param color the color to display the text in
     * @param isAncillary true if the text is ancillary, false if otherwise
     */
    public GUItext(String text, Color color, boolean isAncillary) {
        super(checkNotNull(color));
        this.text = text;
        this.isAncillary = true;
    }
    
    public GUItext(String text, Color activeColor, Color inactiveColor) {
        super(activeColor, inactiveColor);
        this.text = text;
    }
    
    /**
     * Constructor for a textString item with a specified position
     * @param text the string to be displayed
     * @param specX the x position of the textString item
     * @param specY the y position of the textString item
     */
    public GUItext(String text, int specX, int specY) {
        this.text = text;
        //textCodes = Translator.translate(text);
        this.customX = specX;
        this.customY = specY;
    }
    
    /**
     * Constructor for a textString item with a specified position and is ancillary
     * @param text the textString to be displayed
     * @param specX the x position of the textString item
     * @param specY the y position of the textString item
     */
    public GUItext(String text, int specX, int specY, boolean isAncillary) {
        this.text = text;
        this.customX = specX;
        this.customY = specY;
        this.isAncillary = true;
    }
    
    public char charAt(int index) {
        return text.charAt(index);
    }
    
    public void setText(String text) {
        this.text = text;
    }
    
    public void clear() {
        text = "";
    }
    
    public String concat(String str) {
        String newString = text.concat(str);
        text = newString;
        return newString;
    }
    
    public void insertChar(char insertChar, int index) {
        if (index >= 0 && index < text.length()) {
            String leftPart = text.substring(0, index);
            String rightPart = text.substring(index);
            text = leftPart + insertChar + rightPart;
        }
    }
    
    public void removeAll(char ch) {
        text = removeAlls(ch, text);
    }
    
    private String removeAlls(char ch, String str) {
        for(int i = 0; i < str.length(); i++) {
            boolean found = str.charAt(i) == ch;
            if(found && i == str.length() - 1) {
                return "";
            } else if(found) {
                return str.substring(0, i) + removeAlls(ch, str.substring(i + 1));
            }
        }
        return str;
    }
    
    public String concatLeft(String str) {
        String newString = str.concat(text);
        text = newString;
        return newString;
    }

    /**
     * For the given index, return the current Image.
     * @param bitmapContext
     * @param currIndex
     * @param isActive
     * @param activeColor
     * @param inactiveColor
     * @return
     */
    public ImageRepresentation determineCurrImg(BitmapContext bitmapContext, 
            int currIndex, boolean isActive, Color activeColor, Color inactiveColor) {
        checkNotNull(bitmapContext);

        if (isActive){
            return new LetterRepresentation(
                    getFinalActiveColor(activeColor),
                    Color.BLACK, 
                    toString().charAt(currIndex));
        } else {
            return new LetterRepresentation(
                    getFinalInactiveColor(inactiveColor),
                    Color.BLACK,
                    toString().charAt(currIndex));
        }
    }

    /**
     * For the given index, return the current Image.
     * @param bitmapContext
     * @param currIndex
     * @param isActive
     * @param color
     * @return
     */
    public ImageRepresentation determineCurrImg(BitmapContext bitmapContext,
                                                int currIndex, boolean isActive, Color color) {
        return this.determineCurrImg(bitmapContext, currIndex,
                isActive, color, color);
    }
    
    private Color getFinalActiveColor(Color parentActiveColor) {
//        if (this.activeColor != null) {
//            return this.activeColor;
//        } else if (isAncillary) {
//            return DEFAULT_ANCILLARY_TEXT_COLOR;
//        } else if (parentActiveColor != null){
//            return parentActiveColor;
//        } else {
//            return DEFAULT_ACTIVE_COLOR;
//        }
        return parentActiveColor;
    }
    
    private Color getFinalInactiveColor(Color parentInactiveColor) {
//        if (this.inactiveColor != null) {
//            return this.inactiveColor;
//        } else if (isAncillary) {
//            return DEFAULT_ANCILLARY_TEXT_COLOR;
//        } else if (parentInactiveColor != null){
//            return parentInactiveColor;
//        } else {
//            return DEFAULT_INACTIVE_COLOR;
//        }
        return parentInactiveColor;
    }
    
    @Override
    public int getLength() {
        return this.text.length();
    }

    @Override
    public int getHeight() {
        return 1;
    }

    public boolean isAncillary() {
        return isAncillary;
    }
    
    public void setIsAncillary(boolean isAncillary) {
        this.isAncillary = isAncillary;
    }
//    
//    public void setNormal() {
//        this.isAncillary = false;
//    }
    
    public boolean isEmpty() {
        return text.isEmpty();
    }

    public void removeLastChar() {
        text = text.substring(0, text.length() - 1); 
    }
    
    public void removeLeftChar() {
        text = text.substring(1);
    }
    
    @Override
    public String toString(){
        return this.text;
    }

    @Override
    public MutableLayer getGuiOverlay(MutableLayer mainImRepMatrix) {
        return new MutableLayer(mainImRepMatrix.getWidth(),
                mainImRepMatrix.getHeight());
    }

    @Override
    public int compareTo(GUItext guiText) {
        return this.text.compareTo(guiText.text);
    }

//    public int[] getTextCodes() {
//        return this.textCodes;
//    }
}
