package tjp.wiji.gui.gui_element;

import tjp.wiji.drawing.BitmapContext;
import tjp.wiji.representations.*;

import java.awt.*;

import static com.google.common.base.Preconditions.checkNotNull;

public class InputField extends GuiElement {
    // optional
    private Color backgroundColor;
    
    // optional
    private int emptyLength;

    private String emptyPrompt;

    protected final GUItext inProgressText;
    
    protected int maximumLength;
    
    protected InputField(Color activeColor, Color inactiveColor, int maximumLength) {
        super(inactiveColor, activeColor);
        this.inProgressText = new GUItext("");
        this.maximumLength = maximumLength;
    }
    
    public InputField(Color activeColor, Color inactiveColor, 
            int emptyLength, int maximumLength) {
        super(inactiveColor, activeColor);
        this.inProgressText = new GUItext("");
        this.emptyLength = emptyLength;
        this.maximumLength = maximumLength;
    }

    public InputField(Color activeColor, Color inactiveColor, 
            int maximumLength, String emptyPrompt, Color backgroundColor) {

        this(activeColor, inactiveColor, maximumLength);
        this.emptyPrompt = checkNotNull(emptyPrompt);
        this.backgroundColor = backgroundColor;
    }

    public ImageRepresentation determineCurrImg(BitmapContext bitmapContext,
            int currIndex, boolean isActive, Color parentActiveColor, Color parentInactiveColor) {

        checkNotNull(bitmapContext);

        if (isEmpty()) {
            if (emptyPrompt != null) {
                return handleEmptyPrompt(bitmapContext, currIndex, isActive);
            } else {
                return handleEmptyBlank(bitmapContext, isActive);
            }
        } else {
            return handleNonEmpty(currIndex, isActive);
        }
    }

    @Override
    public int getLength() {
        if (isEmpty()) {
            if (emptyPrompt != null) {
                return emptyPrompt.length();   
            } else {
                return emptyLength;
            }
        } else {
            return inProgressText.getLength();
        }
    }

    @Override
    public int getHeight() {
        return 1;
    }

    private ImageRepresentation handleEmptyBlank(BitmapContext bitmapContext, boolean isActive) {
        if (isActive){
            return new ImageRepresentation(
                    this.activeColor,
                    Color.BLACK, 
                    Graphic.FILLED_CELL);
        } else {
            checkNotNull(this.inactiveColor);
            return new ImageRepresentation(
                    this.inactiveColor,
                    Color.BLACK,
                    Graphic.FILLED_CELL);
        }
    }

    private ImageRepresentation handleEmptyPrompt(BitmapContext bitmapContext, 
            int currIndex, boolean isActive) {
        char currChar = emptyPrompt.charAt(currIndex);
        
        if (isActive){
            return new LetterRepresentation(
                    this.activeColor,
                    backgroundColor, 
                    currChar);
        } else {
            checkNotNull(this.inactiveColor);
            return new LetterRepresentation(
                    this.inactiveColor,
                    backgroundColor,
                    currChar);
        }
    }
    
    private ImageRepresentation handleNonEmpty(
            int currIndex, boolean isActive) {
        
        if (isActive){
            return new LetterRepresentation(
                    this.activeColor,
                    Color.BLACK, 
                    inProgressText.charAt(currIndex));
        } else {
            checkNotNull(this.inactiveColor);
            return new LetterRepresentation(
                    this.inactiveColor,
                    Color.BLACK,
                    inProgressText.charAt(currIndex));
        }
    }
    
    public void insertChar(char insertChar, int index) {
        inProgressText.insertChar(insertChar, index);
    }

    public boolean isAncillary() {
        return false;
    }
    
    public boolean isEmpty() {
        return inProgressText.isEmpty();
    }
    
    @Override
    public MutableLayer getGuiOverlay(MutableLayer mainImRepMatrix) {
        return new MutableLayer(mainImRepMatrix.getWidth(),
                mainImRepMatrix.getHeight());
    }
    
    public void pop() {
        if (!isEmpty()) {
            inProgressText.removeLastChar();
        }
    }
    
    public void push(char newChar) {
        if (inProgressText.getLength() <= maximumLength) {
            inProgressText.concat(Character.toString(newChar));
        }
    }
    
    public void pushLeft(char newChar) {
        if (inProgressText.getLength() <= maximumLength) {
            inProgressText.concatLeft(Character.toString(newChar));
        }
    }

    public void removeAll(char ch) {
        inProgressText.removeAll(ch);
    }

    protected void setEmptyPrompt(String emptyPrompt) {
        this.emptyPrompt = emptyPrompt;
    }

    @Override
    public String toString() {
        return inProgressText.toString();
    }
}
