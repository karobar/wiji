package tjp.wiji.gui.gui_element;

import tjp.wiji.drawing.BitmapContext;
import tjp.wiji.representations.ImageRepresentation;
import tjp.wiji.representations.MutableLayer;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Make a list of objects bounded to a list of their names
 * @param <T>
 */
public class ModelObjectCollection<T> extends ParentTextCollection {
    private final NavigableMap<GUItext, T> logicalObjectMap = new TreeMap<>();

    private final List<GUItext> ancillaryTextList = new ArrayList<GUItext>();

    public ModelObjectCollection(BitmapContext bitmapContext, Color inactive,
                                 Color active, int x, int y) {
        super(bitmapContext, inactive, active, x, y, false);
    }

    public ModelObjectCollection(BitmapContext bitmapContext, int x, int y) {
        super(bitmapContext, x, y, false);
    }

    /**
     * Adds a GUIText element along with its corresponding Factory to the
     * TextCollection.
     * For example, makeNewWithAdded a new GUIText with the textString "Torch" along with a
     * specific Factory with the textString Torch.  This is useful for
     * the inventory screen, for example. This providese a tie between the GUI
     * textString object and a logical game object.
     * @param text the text to be displayed
     * @param obj the object corresponding to the textString.
     */
    public void add(GUItext text, T obj) {
        logicalObjectMap.put(text, obj);
    }

    public void addAncillary(GUItext ancillaryText) {
        ancillaryTextList.add(ancillaryText);
    }

    public void clearLogicalObjectMap() {
        logicalObjectMap.clear();
    }

    public void cycleUp() {
        // TODO
    }

    public void cycleDown() {
        // TODO
    }

    public boolean isAncillary() {
        return false;
    }

    @Override
    protected GUItext get(int requestedIndex) {
        NavigableSet<GUItext> keys = this.logicalObjectMap.descendingKeySet();

        int index = 0;
        for(GUItext key : keys) {
            if (index == requestedIndex) {
                return key;
            }
            index++;
        }
        throw new IndexOutOfBoundsException("Tried to index this ModelObjectCollection with index " +
                requestedIndex + ", but the size of the list is " + keys.size());
    }

    private void displayCurrentElement(GUItext currText, int displayAreaWidth,
                                       int displayAreaHeight, int currentY,
                                       boolean isActive, MutableLayer overlay,
                                       Color activeColor, Color inactiveColor) {
        // This is in screen-space
        int currentX;

        if (this.centered) {
            currentX = (displayAreaWidth - currText.getLength()) / 2;
        } else {
            currentX = this.screenX;
        }

        for (int j = 0; j < currText.getLength(); j++) {

            ImageRepresentation currentImg = currText.determineCurrImg(
                    bitmapContext,
                    j,
                    isActive,
                    activeColor, inactiveColor
            );

            int x = (currText.customX >= 0) ? currText.customX + j : currentX;
            int y = (currText.customY >= 0) ? currText.customY : currentY;
            if (x < displayAreaWidth && x >= 0 &&
                    y < displayAreaHeight && y >= 0) {

                overlay.put(currentImg, x, y);
            }
            currentX++;
        }
    }

    /**
     * Overwrites screen squares with the choiceList.
     * PRECONDITION:  Given a display area (usually MainScreen.mainImRepMatrix)
     * POSTCONDITION: (Over)writes ImageRepresentations onto the given display
     * matrix, taken from the ints translated from all
     * GUIText.textString within the TextCollection
     *
     * @param displayArea the displayArea which will be over-written
     * @return
     */
    @Override
    public MutableLayer getGuiOverlay(MutableLayer displayArea) {
        MutableLayer overlay = new MutableLayer(displayArea.getWidth(),
                displayArea.getWidth());

        int currentY = this.screenY;
        //cycle through all the standard GUI elements
        for (int GuiElementIndex = 0;
             GuiElementIndex < logicalObjectMap.size();
             GuiElementIndex++) {

            int currIndex = GuiElementIndex + calculateOffset();

            boolean isActive = currIndex == currentChoiceIndex;

            displayCurrentElement(get(currIndex), displayArea.getWidth(),
                    displayArea.getHeight(), currentY, isActive,
                    overlay, this.activeColor, this.inactiveColor);

            currentY++;
        }

        //cycle through all the ancillary GUI elements
        for (int ancillaryTextIndex = 0;
             ancillaryTextIndex < ancillaryTextList.size();
             ancillaryTextIndex++) {

            int currIndex = ancillaryTextIndex + calculateOffset();

            boolean isActive = currIndex == currentChoiceIndex;

            displayCurrentElement(ancillaryTextList.get(currIndex),
                    displayArea.getWidth(),displayArea.getHeight(),
                    currentY, isActive, overlay, Color.GRAY,
                    Color.GRAY);

            currentY++;
        }
        return overlay;
    }

    @Override
    public int getLength() {
        return 0;
    }

    public int getHeight() { return logicalObjectMap.size() + ancillaryTextList.size(); }

    @Override
    protected int calculateOffset() {
        return 0;
    }

    /**
     * Returns the logical object which is referred to by the GUIText which is
     * the current choice.
     * @return the logical game object of the current choice
     */
    public T getCurrentLogicalObject() {
        GUItext key = get(currentChoiceIndex);
        T value = logicalObjectMap.get(key);
        return value;
    }
}
