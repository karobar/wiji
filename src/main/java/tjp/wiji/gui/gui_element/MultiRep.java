package tjp.wiji.gui.gui_element;

import tjp.wiji.representations.Layer;

public interface MultiRep {
    Layer getReps();
}
