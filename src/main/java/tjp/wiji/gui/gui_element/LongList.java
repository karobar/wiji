package tjp.wiji.gui.gui_element;

import tjp.wiji.drawing.BitmapContext;

import java.awt.*;
import java.util.LinkedHashMap;

/**
 * A LongList is a List which is longer than the screen can display.
 * @param <T>
 */
public class LongList<T extends Object> extends ParentTextCollection  {
    public static class LongListBuilder<T> {
        private BitmapContext bitmapContext;
        //        private int width = 0;
//        private int height = 0;
        private boolean centered = false;
        //private CellDimensions dims;
        private int endY;
        private Color inactiveColor, activeColor, tertiaryColor;
        private String initialItem;
        private int x, y;
        
        public LongListBuilder<T> activeColor(Color activeColor) {
            this.activeColor = activeColor;
            return this;
        }
        
        public LongListBuilder<T> bitmapContext(BitmapContext bitmapContext) {
            this.bitmapContext = bitmapContext;
            return this;
        }
        
        /**
         * TODO: add all sorts of checks to make sure required combinations
         * are provided in full.
         * @return
         */
        public  LongList<T> build() {
            Color sendOffActiveColor;
            Color sendOffInactiveColor;

            sendOffActiveColor = this.activeColor != null ?
                    activeColor  : GuiElement.DEFAULT_ACTIVE_COLOR;
            sendOffInactiveColor = this.inactiveColor != null ?
                    inactiveColor  : GuiElement.DEFAULT_INACTIVE_COLOR;
            return new LongList<T>(
                    this.bitmapContext,
                    sendOffActiveColor,
                    sendOffInactiveColor,
                    this.x, this.y, this.endY,
                    this.tertiaryColor,
                    this.centered);
        }
        
        public LongListBuilder<T> centered() {
            this.centered = true;
            return this;
        }
        
        public LongListBuilder<T> color(Color color) {
            this.inactiveColor = color;
            this.activeColor = color;
            return this;
        }
        
        public LongListBuilder<T> endY(int endY) {
            this.endY = endY;
            return this;
        }
        
        public LongListBuilder<T> inactiveColor(Color inactiveColor) {
            this.inactiveColor = inactiveColor;
            return this;
        }
        
        public LongListBuilder<T> initialItem(String initialItem) {
            this.initialItem = initialItem;
            return this;
        }
        
        public LongListBuilder<T> tertiaryColor(Color tertiaryColor) {
            this.tertiaryColor = tertiaryColor;
            return this;
        }
        
        public LongListBuilder<T> x(int x) {
            this.x = x;
            return this;
        }
        
        public LongListBuilder<T> y(int y) {
            this.y = y;
            return this;
        }
    }  
    
    public static <T> LongListBuilder<T> newLongListBuilder() {
        return new LongListBuilder<T>();
    }
    private final int endY;
    
    private final LinkedHashMap<GuiElement, T> objMap = new LinkedHashMap<GuiElement, T>();

    private final Color tertiaryColor;

    protected LongList(BitmapContext bitmapContext, Color activeColor, Color inactiveColor,
                       int x, int y, int endY, Color tertiaryColor,boolean centered) {
        super(bitmapContext, activeColor, inactiveColor, x, y, centered);
        this.endY = endY;
        this.tertiaryColor = tertiaryColor;
    }
    
    public void add(GuiElement element, T tiedObj) {
        objMap.put(element, tiedObj);
    }

    @Override
    protected int calculateOffset() {
        if (endY == 0) {
            return 0;
        }
        
        int topOfScreen = this.screenY;
        
        int totalHeightOnScreen = getListHeight();
        int halfHeight = totalHeightOnScreen / 2;
        if (currentChoiceIndex > getHeight() - halfHeight) {
            topOfScreen = getHeight() - totalHeightOnScreen + 1;
        } else if (currentChoiceIndex > halfHeight) {
            topOfScreen = currentChoiceIndex - halfHeight;
        } 
        return topOfScreen - 1;
    }

    public void cycleDown() {
        currentChoiceIndex = (currentChoiceIndex + 1) % objMap.size();
        if (get(currentChoiceIndex).isAncillary()) {
            cycleDown();
        }
        cycleDownHook();
    }

    protected void cycleDownHook() { }   
    
    /**
     * Moves the current choice index up (or left), wraps around.
     */
    public void cycleUp() {
        if(currentChoiceIndex > 0) {
            currentChoiceIndex = currentChoiceIndex - 1;
        }
        else if (currentChoiceIndex == 0){
            currentChoiceIndex = objMap.size()-1;
        }
        else {
            System.out.println("yo, cycleActiveUp in ChoiceList is bein' wierd");
        }     
        if (get(currentChoiceIndex).isAncillary()) {
            cycleUp();
        }
        cycleUpHook();
    }

    protected void cycleUpHook() { }

    @Override
    public GUItext get(int index) {
        return (GUItext) objMap.keySet().toArray()[index];
    }
    
    public T getCurrentElement() {
        return objMap.get(getCurrentChoice());
    }

    public GUItext getCurrentChoice() {
        return get(currentChoiceIndex);
    }
    
    public T getFromKey(GuiElement key) {
        return objMap.get(key);
    }

    @Override
    public int getHeight() {
        return objMap.size();
    }

    protected int getListHeight() {
        if (endY > 0) {
            return endY - this.screenY;
        } else {
            return getHeight();
        }
    }

    boolean isCentered() {
        return false;
    }
}
