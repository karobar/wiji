package tjp.wiji.gui.gui_element;

import tjp.wiji.drawing.BitmapContext;
import java.awt.Color;
import tjp.wiji.gui.Cyclable;
import tjp.wiji.representations.ImageRepresentation;
import tjp.wiji.representations.MutableLayer;

public abstract class ParentTextCollection extends GuiElement implements Cyclable {

    protected final BitmapContext bitmapContext;
    protected final int screenX;
    protected final int screenY;
    protected final boolean centered;
    protected int currentChoiceIndex;
    protected GUItext tertiaryElement;

    protected ParentTextCollection(BitmapContext bitmapContext,
                                   Color activeColor, Color inactiveColor,
                                   int screenX, int screenY, boolean centered) {
        super(inactiveColor, activeColor);
        this.bitmapContext = bitmapContext;
        this.screenX = screenX;
        this.screenY = screenY;
        this.centered = centered;
    }

    protected ParentTextCollection(BitmapContext bitmapContext,
                                   int screenX, int screenY, boolean centered) {
        this(bitmapContext, DEFAULT_ACTIVE_COLOR, DEFAULT_INACTIVE_COLOR,
                screenX, screenY, centered);
    }

    /**
     * Overwrites screen squares with the choiceList.
     * PRECONDITION:  Given a display area (usually MainScreen.mainImRepMatrix)
     * POSTCONDITION: (Over)writes ImageRepresentations onto the given display
     * matrix, taken from the ints translated from all
     * GUIText.textString within the TextCollection
     *
     * @param displayArea the displayArea which will be over-written
     * @return
     */
    @Override
    public MutableLayer getGuiOverlay(MutableLayer displayArea) {
        int currentY = this.screenY;
        MutableLayer overlay = new MutableLayer(displayArea.getWidth(),
                displayArea.getWidth());

        //cycle through all the GUI elements
        // System.out.println("Displaying with height: " + getHeight());
        for (int i = 0; i < getHeight(); i++) {
            int currIndex = i + calculateOffset();
            GUItext currText = get(currIndex);

            // This is in screen-space
            int currentX;

            if (this.centered) {
                currentX = (displayArea.getWidth() - currText.getLength()) / 2;
            } else {
                currentX = this.screenX;
            }

            for (int j = 0; j < currText.getLength(); j++) {
                // System.out.println("checking isActive: " + currIndex + " ?= " + currentChoiceIndex);
                boolean isActive = currIndex == currentChoiceIndex;
                //boolean isTertiary = (get(finalI) == tertiaryElement);

                ImageRepresentation currentImg = currText.determineCurrImg(
                        bitmapContext,
                        j,
                        isActive,
                        this.activeColor, this.inactiveColor
                );

                int x = (currText.customX >= 0) ? currText.customX + j : currentX;
                int y = (currText.customY >= 0) ? currText.customY : currentY;
                if (x < displayArea.getWidth() && x >= 0 &&
                        y < displayArea.getHeight() && y >= 0) {

                    overlay.put(currentImg, x, y);
                }
                currentX++;
            }
            currentY++;
        }
        return overlay;
    }

    protected abstract GUItext get(int i);

    protected abstract int calculateOffset();

    public int getLength() {
        int max = this.get(0).getLength();
        for (int i = 0; i < this.getHeight(); i++) {
            if (this.get(i).getLength() > max) {
                max = this.get(i).getLength();
            }
        }
        return max;
    }
}
