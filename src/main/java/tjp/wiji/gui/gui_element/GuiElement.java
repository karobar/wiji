package tjp.wiji.gui.gui_element;

import tjp.wiji.drawing.BitmapContext;
import tjp.wiji.representations.MutableLayer;

import java.awt.*;

import static com.google.common.base.Preconditions.checkNotNull;

@Deprecated
public abstract class GuiElement {
    /**
     * Gets the width of the GuiElement in cell numbers (integer)
     * @return
     */
    public abstract int getLength();

    public abstract int getHeight();
    
    final static Color DEFAULT_INACTIVE_COLOR = Color.WHITE;
    final static Color DEFAULT_ACTIVE_COLOR   = Color.YELLOW;

    public int customX = -1;
    public int customY = -1;
    
    protected final Color activeColor;
    protected final Color inactiveColor;

    protected GuiElement() {
        this(DEFAULT_INACTIVE_COLOR, DEFAULT_ACTIVE_COLOR);
    }

    protected GuiElement(Color color) {
        this(checkNotNull(color), checkNotNull(color));
    }

    protected GuiElement(Color inactiveColor, Color activeColor) {
        this.inactiveColor = checkNotNull(inactiveColor);
        this.activeColor = checkNotNull(activeColor);
    }
    
    /**
     * Overlays the TextCollection onto the specified Representation grid.
     * For ScreenText, this is simply a displayOnto. For MapText, the text must
     * first be translated from map-space to screen-space.
     * @param mainImRepMatrix a RepresentationField
     * @return
     */
    public abstract MutableLayer getGuiOverlay(MutableLayer mainImRepMatrix);
}
