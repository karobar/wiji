package tjp.wiji.gui.gui_element.placers;

class CenteringGuide implements Guide {
    @Override
    public int getValue(final int myWidth, final int underlayWidth) {
        float center = underlayWidth / 2.0f;
        float myCenter = myWidth / 2.0f;
        return Math.round(center - myCenter);
    }
}
