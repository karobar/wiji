package tjp.wiji.gui.gui_element.placers;

public class IntGuide implements Guide {
    private final int value;

    public IntGuide(final int value) {
        this.value = value;
    }

    public int getValue(final int myWidth, final int underlayWidth) {
        return this.value;
    }
}
