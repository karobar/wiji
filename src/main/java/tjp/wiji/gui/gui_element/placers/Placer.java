package tjp.wiji.gui.gui_element.placers;

import tjp.wiji.representations.Layer;

public interface Placer {
    /**
     *
     * @param underlay  The thing you're placing over
     */
    Layer place(Layer underlay);
}
