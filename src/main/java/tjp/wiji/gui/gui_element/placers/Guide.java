package tjp.wiji.gui.gui_element.placers;

interface Guide {
    int getValue(final int myWidth, final int underlayWidth);
}
