package tjp.wiji.gui.gui_element.placers;

import tjp.wiji.gui.gui_element.MultiRep;
import tjp.wiji.representations.Layer;

/**
 * Places GUI elements.
 */
public class CoordPlacer implements Placer {
    private final MultiRep multiRep;
    private final Guide xGuide;
    private final Guide yGuide;

    /**
     *
     * @param multiRep the thing to place
     */
    public CoordPlacer(final MultiRep multiRep) {
        this(multiRep, 0, 0);
    }

    /**
     *
     * @param multiRep the thing to place
     * @param xOffset
     * @param yOffset
     */
    public CoordPlacer(final MultiRep multiRep, int xOffset, int yOffset) {
        this(multiRep, new IntGuide(xOffset), new IntGuide(yOffset));
    }

    public static CoordPlacer centerX(final MultiRep multiRep, int yOffset) {
        return new CoordPlacer(
                multiRep, new CenteringGuide(), new IntGuide(yOffset));
    }

    public static CoordPlacer centerY(final MultiRep multiRep, int xOffset) {
        return new CoordPlacer(
                multiRep, new IntGuide(xOffset), new CenteringGuide());
    }

    public static CoordPlacer centerXandY(final MultiRep multiRep) {
        return new CoordPlacer(
                multiRep, new CenteringGuide(), new CenteringGuide());
    }

    private CoordPlacer(final MultiRep multiRep, final Guide xOffset, final Guide yOffset) {
        this.multiRep = multiRep;
        this.xGuide = xOffset;
        this.yGuide = yOffset;
    }

    @Override
    public Layer place(final Layer underlay) {
        final Layer myReps = this.multiRep.getReps();

        return myReps.overlay(underlay,
                xGuide.getValue(myReps.getWidth(), underlay.getWidth()),
                yGuide.getValue(myReps.getHeight(), underlay.getHeight()));
    }
}
