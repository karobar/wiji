package tjp.wiji.gui.gui_element;

import tjp.wiji.representations.ImageRepresentation;
import tjp.wiji.representations.ImmutableLayer;
import tjp.wiji.representations.Layer;

import java.awt.*;
import java.util.ArrayList;

public class NewGuiText implements MultiRep {
    private final String text;
    private final Color color;

    public NewGuiText(final String text) {
        this.text = text;
        this.color = Color.WHITE;
    }

    public NewGuiText(final String text, Color color) {
        this.text = text;
        this.color = color;
    }

    @Override
    public Layer getReps() {
        ArrayList<ImageRepresentation> reps = new ArrayList<>();
        for (char letter : text.toCharArray()) {
            reps.add(new ImageRepresentation(letter, color));
        }
        return ImmutableLayer.row(reps);
    }
}
