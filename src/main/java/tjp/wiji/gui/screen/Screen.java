package tjp.wiji.gui.screen;

import com.google.common.collect.ImmutableSet;
import tjp.wiji.drawing.CellExtents;
import tjp.wiji.event.EventProcessable;
import tjp.wiji.gui.gui_element.GuiElement;
import tjp.wiji.gui.gui_element.placers.Placer;
import tjp.wiji.representations.ImageRepresentation;
import tjp.wiji.representations.MutableLayer;
import tjp.wiji.representations.Layer;

import java.util.Collection;

/** 
 * A screen is a logical representation of a series of graphical elements and 
 * corresponding control instructions.
 *
 * TODO: split up control instructions to its own class
 * 
 * @author      Travis Pressler (travisp471@gmail.com)
 * @version     %I%, %G%
 */
public abstract class Screen implements EventProcessable {
    //how long an animation frame lasts
    final private long FRAME_TIME_IN_MS = 1000;
    
    //one millisecond is 1000 nanoseconds
    final private long NANO_TO_MS_FACTOR = 1000;

    private long nanosSinceLastRender = 0;

    final private CellExtents cellExtents;
    
    private final int MAX_MISSED_FRAMES = 10;

    protected final ScreenChangeController screenChangeController;

    protected Screen(final ScreenChangeController screenChangeController) {
        this(CellExtents.DEFAULT, screenChangeController);
    }

    protected Screen(final CellExtents cellExtents,
                     final ScreenChangeController screenChangeController) {
        this.cellExtents = cellExtents;
        this.screenChangeController = screenChangeController;
    }

    protected void stepScreenBackwards() {
        screenChangeController.stepScreenBackwards();
    }

    protected void stepScreenForwards(final Screen newScreen) {
        screenChangeController.stepScreenForwards(newScreen);
    }

    @Deprecated
    protected abstract Collection<GuiElement> getGuiElements();

    /**
     * Override this.
     * @return
     */
    protected ImmutableSet<Placer> getGuiReps() {
        return ImmutableSet.of();
    }

    protected CellExtents getCellExtents() {
        return cellExtents;
    }

    @SuppressWarnings("unused")
    public int getWidthInCells() {
        return cellExtents.getWidthInSlots();
    }

    @SuppressWarnings("unused")
    public int getHeightInCells() {
        return cellExtents.getHeightInSlots();
    }

    /**
     *
     * @param x horizontal offset from the left edge
     * @param y vertical offset from the top edge
     * @return
     */
    protected abstract ImageRepresentation getCurrentCell(int x, int y);
    
    protected abstract void handleFrameChange();
    
    /**
     * Gets all GUI elements (both screen and map relative) and overrides 
     * the game element representations which are overlaid.
     * @param mainImRepMatrix the Representation field to overlay
     */
    @Deprecated
    private void overlayGUI(MutableLayer mainImRepMatrix) {
        for(GuiElement element : getGuiElements()) {
            MutableLayer overlay = element.getGuiOverlay(
                    mainImRepMatrix);

            mainImRepMatrix.overlay(overlay);
        }
    }

    private Layer runGuiPlacers(final Layer mainImRepMatrix) {
        Layer currentField = mainImRepMatrix;
        for (Placer placer : getGuiReps()) {
            currentField = placer.place(currentField);
        }
        return currentField;
    }
    
    /**
     * Load all cells into the main image representation matrix.
     */
    private void prepareReps(MutableLayer mainImRepMatrix) {
        for(int x = 0; x < mainImRepMatrix.getWidth(); x++) {
            for(int y = 0; y < mainImRepMatrix.getHeight(); y++) {
                mainImRepMatrix.put(getCurrentCell(x,y), x, y);
            }
        }
    }
    
    /**
     * Create a 2d array of ImageRepresentations which will form the final output to the screen.
     * @param inLastRenderTime
     * @return 
     */
    public Layer render(long inLastRenderTime, int width, int height) {
        nanosSinceLastRender += inLastRenderTime;
        //this handles animation frame changes
        sendFrameChangeEvery(NANO_TO_MS_FACTOR * FRAME_TIME_IN_MS);
        MutableLayer mainImRepMatrix = new MutableLayer(width,height);
        //first, prepare the representations (add them to mainImRepMatrix)
        prepareReps(mainImRepMatrix);
        //then, overlay all active GUI elements (add them to mainImRepMatrix,
        //overwriting the prepared reps)
        overlayGUI(mainImRepMatrix);
        return runGuiPlacers(mainImRepMatrix);
    }
    
    protected void sendFrameChangeEvery(long frameTime) {
        long frames = nanosSinceLastRender / frameTime;
        if (frames > 0) {
            if (frames > MAX_MISSED_FRAMES) {
                frames = MAX_MISSED_FRAMES; 
            }
            for (int i = 0; i < frames; i++) {
                handleFrameChange();
            }
            nanosSinceLastRender = 0;
        }
    }
    
    public abstract void stepToScreenTrigger();
}
