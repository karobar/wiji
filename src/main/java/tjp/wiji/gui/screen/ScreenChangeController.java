package tjp.wiji.gui.screen;

public interface ScreenChangeController {
    /**
     * Used when exiting a screen to make sure that all pointers decrement by
     * one.
     */
    void stepScreenBackwards();

    /**
     * used when creating a new screen to make sure that the user can return to
     * the screen they were just at.
     */
    void stepScreenForwards(Screen newScreen);
}
