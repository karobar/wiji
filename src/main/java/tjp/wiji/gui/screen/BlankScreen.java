package tjp.wiji.gui.screen;

import com.google.common.collect.ImmutableSet;
import tjp.wiji.event.GameEvent;
import tjp.wiji.gui.gui_element.GuiElement;
import tjp.wiji.representations.ImageRepresentation;

import java.util.Collection;

public class BlankScreen extends Screen {
    public BlankScreen(final ScreenChangeController screenChangeController) {
        super(screenChangeController);
    }

    @Override
    protected Collection<GuiElement> getGuiElements() {
        return ImmutableSet.of();
    }

    @Override
    protected ImageRepresentation getCurrentCell(int width, int j) {
        return ImageRepresentation.ALL_BLACK;
    }

    @Override
    protected void handleFrameChange() {

    }

    @Override
    public void stepToScreenTrigger() {

    }

    @Override
    public void handleEvent(GameEvent e) {

    }
}
