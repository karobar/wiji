package tjp.wiji.gui.screen;

import tjp.wiji.event.GameEvent;

/**
 * A bag of screens
 * TODO: make immutable
 * @author travis
 */
public class ScreenContext implements ScreenChangeController {
    private Screen greatGrandparentScreen;
    private Screen grandparentScreen;
    private Screen previousScreen;
    private Screen currentScreen;

    /**
     * Create a ScreenContext which only starts with a blank screen, and cannot move to another
     */
    public static ScreenContext create() {
        ScreenChangeController delegate = new ScreenChangeController() {
            @Override
            public void stepScreenBackwards() { }

            @Override
            public void stepScreenForwards(Screen newScreen) { }
        };

        return create(new BlankScreen(delegate));
    }

    public static ScreenContext create(final Screen startingScreen) {
        final ScreenContext screenContext = new ScreenContext(startingScreen);
        screenContext.currentScreen.stepToScreenTrigger();
        return screenContext;
    }
    
    private ScreenContext(final Screen startingScreen) {
        this.currentScreen = startingScreen;
        this.previousScreen = startingScreen;
        this.grandparentScreen = startingScreen;
        this.greatGrandparentScreen = startingScreen;
    }

    public void handleEvent(final GameEvent e) {
        this.currentScreen.handleEvent(e);
    }
    
    @Override
    public void stepScreenBackwards() {
        currentScreen  = previousScreen;
        previousScreen = grandparentScreen;
        grandparentScreen = greatGrandparentScreen;
        currentScreen.stepToScreenTrigger();
    }

    public void shadowStepForwards(Screen newScreen) {
        greatGrandparentScreen = grandparentScreen;
        grandparentScreen = previousScreen;
        currentScreen = newScreen;
        currentScreen.stepToScreenTrigger();
    }

    @Override
    public void stepScreenForwards(Screen newScreen) {
        greatGrandparentScreen = grandparentScreen;
        grandparentScreen = previousScreen;
        previousScreen = currentScreen;
        currentScreen = newScreen;
        currentScreen.stepToScreenTrigger();
    }
    
    public Screen getCurrentScreen() {
        return currentScreen;
    }
}
