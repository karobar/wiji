package tjp.wiji.file.beans;

/**
 * filename: Dullard_Exponent_12x12.bmp
 * control_color: 0xFF00FF
 */
public class TileSetDescription {
    private String filename;
    private int controlColor;
    private int charWidth;
    private int charHeight;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public int getControlColor() {
        return controlColor;
    }

    public void setControlColor(int controlColor) {
        this.controlColor = controlColor;
    }

    public int getCharWidth() {
        return charWidth;
    }

    public void setCharWidth(int charWidth) {
        this.charWidth = charWidth;
    }

    public int getCharHeight() {
        return charHeight;
    }

    public void setCharHeight(int charHeight) {
        this.charHeight = charHeight;
    }
}
