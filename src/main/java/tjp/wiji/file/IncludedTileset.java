package tjp.wiji.file;

/**
 * This is currently broken, don't use it.
 */
public enum IncludedTileset {
    DULLARD_EXPONENT_12X12("Dullard_Exponent_12x12.yaml");

    final String path;
    IncludedTileset(final String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
