package tjp.wiji.file;

import com.badlogic.gdx.files.FileHandle;
import com.google.common.annotations.VisibleForTesting;
import tjp.wiji.drawing.bootstrap.FilesModule;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class FileLoaderUtils {
    private FileLoaderUtils() {}

    private static URL getResource(String resource) {
        final List<ClassLoader> classLoaders = new ArrayList<ClassLoader>();
        classLoaders.add(Thread.currentThread().getContextClassLoader());
        classLoaders.add(FileLoaderUtils.class.getClassLoader());

        for (ClassLoader classLoader : classLoaders) {
            final URL url = getResourceWith(classLoader, resource);
            if (url != null) {
                return url;
            }
        }

        final URL systemResource = ClassLoader.getSystemResource(resource);
        if (systemResource != null) {
            return systemResource;
        } else {
            try {
                return new File(resource).toURI().toURL();
            } catch (MalformedURLException e) {
                return null;
            }
        }
    }

    private static URL getResourceWith(ClassLoader classLoader, String resource) {
        if (classLoader != null) {
            return classLoader.getResource(resource);
        }
        return null;
    }
    
    public static FileHandle getFileHandle(String URL) throws URISyntaxException {
        URL path = getResource(URL);
        File charFile = new File(path.toURI());
        return new FileHandle(charFile);
    }

    public static BufferedImage loadPng(final String path, final FilesModule files) throws IOException {
        FileHandle handle = files.internal(path);
        InputStream stream = handle.read();
        return ImageIO.read(stream);
    }
}
