package tjp.wiji.gui;

import org.junit.Test;
import tjp.wiji.drawing.bootstrap.AppBootstrapper;
import tjp.wiji.file.FileLoaderUtils;
import tjp.wiji.representations.ImageRepresentation;
import tjp.wiji.representations.ImmutableLayer;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ImmutableLayerTest {
    private static BufferedImage getSampleImage() throws IOException {
        return FileLoaderUtils.loadPng("sample_transparency.png",
                AppBootstrapper.create().getFilesModule());
    }

    @Test
    public void testLoadFromTransparencyHasRep() throws IOException {
        ImmutableLayer layer = ImmutableLayer.fromTransparency(getSampleImage());
        assertTrue(layer.get(0,0).isPresent());
    }

    @Test
    public void testLoadFromTransparencyHasAlphaRep() throws IOException {
        ImmutableLayer layer = ImmutableLayer.fromTransparency(getSampleImage());
        ImageRepresentation rep = layer.get(0,0).get();
        assertEquals(0, rep.getForeColor().getAlpha());
        assertEquals(0, rep.getBackColor().getAlpha());
    }

    @Test
    public void testLoadFromTransparencyHasNonAlphaRep() throws IOException {
        ImmutableLayer layer = ImmutableLayer.fromTransparency(getSampleImage());
        // This pixel should be in the 'S' of 'SAMPLE'
        ImageRepresentation rep = layer.get(30,34).get();
        assertEquals(Color.WHITE, rep.getForeColor());
        assertEquals(Color.WHITE, rep.getBackColor());
    }
}
