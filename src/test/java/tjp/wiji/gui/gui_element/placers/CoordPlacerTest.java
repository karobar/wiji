package tjp.wiji.gui.gui_element.placers;

import com.google.common.collect.ImmutableList;
import org.junit.Test;
import tjp.wiji.gui.gui_element.NewGuiText;
import tjp.wiji.representations.Graphic;
import tjp.wiji.representations.ImageRepresentation;
import tjp.wiji.representations.ImmutableLayer;
import tjp.wiji.representations.Layer;

import static org.junit.Assert.assertEquals;

public class CoordPlacerTest {
    @Test
    public void basicPlaceTest() {
        CoordPlacer placer = new CoordPlacer(new NewGuiText("fef"));
        Layer underlay = ImmutableLayer.row(ImmutableList.of(
                new ImageRepresentation(1),
                new ImageRepresentation(2),
                new ImageRepresentation(3),
                new ImageRepresentation(4),
                new ImageRepresentation(5)
        ));

        Layer outlayer = placer.place(underlay);

        assertEquals(Graphic.LOWERCASE_F.getRawImageChar(),
                outlayer.get(0,0).get().getImgChar());
        assertEquals(Graphic.LOWERCASE_E.getRawImageChar(),
                outlayer.get(1,0).get().getImgChar());
        assertEquals(Graphic.LOWERCASE_F.getRawImageChar(),
                outlayer.get(2,0).get().getImgChar());
        assertEquals(4,
                outlayer.get(3,0).get().getImgChar());
        assertEquals(5,
                outlayer.get(4,0).get().getImgChar());
    }

    @Test
    public void basicCenteringTest() {
        CoordPlacer placer = CoordPlacer.centerX(new NewGuiText("fef"), 0);
        Layer underlay = ImmutableLayer.row(ImmutableList.of(
                new ImageRepresentation(1),
                new ImageRepresentation(2),
                new ImageRepresentation(3),
                new ImageRepresentation(4),
                new ImageRepresentation(5)
        ));

        Layer outlayer = placer.place(underlay);

        assertEquals(1,
                outlayer.get(0,0).get().getImgChar());
        assertEquals(Graphic.LOWERCASE_F.getRawImageChar(),
                outlayer.get(1,0).get().getImgChar());
        assertEquals(Graphic.LOWERCASE_E.getRawImageChar(),
                outlayer.get(2,0).get().getImgChar());
        assertEquals(Graphic.LOWERCASE_F.getRawImageChar(),
                outlayer.get(3,0).get().getImgChar());
        assertEquals(5,
                outlayer.get(4,0).get().getImgChar());
    }

    @Test
    public void centeringBothTest() {
        CoordPlacer placer = CoordPlacer.centerXandY(new NewGuiText("fef"));
        Layer underlay = ImmutableLayer.row(ImmutableList.of(
                new ImageRepresentation(1),
                new ImageRepresentation(2),
                new ImageRepresentation(3),
                new ImageRepresentation(4),
                new ImageRepresentation(5)
        ));

        Layer outlayer = placer.place(underlay);

        assertEquals(1,
                outlayer.get(0,0).get().getImgChar());
        assertEquals(Graphic.LOWERCASE_F.getRawImageChar(),
                outlayer.get(1,0).get().getImgChar());
        assertEquals(Graphic.LOWERCASE_E.getRawImageChar(),
                outlayer.get(2,0).get().getImgChar());
        assertEquals(Graphic.LOWERCASE_F.getRawImageChar(),
                outlayer.get(3,0).get().getImgChar());
        assertEquals(5,
                outlayer.get(4,0).get().getImgChar());
    }
}