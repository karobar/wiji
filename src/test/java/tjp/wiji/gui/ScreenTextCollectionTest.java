package tjp.wiji.gui;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

import tjp.wiji.drawing.BitmapContext;
import tjp.wiji.gui.gui_element.ScreenTextList;

import java.awt.*;

public class ScreenTextCollectionTest {
    // Integration
    @Test
    public void testBuilder() {
        ScreenTextList dut = new ScreenTextList.Builder()
                .bitmapContext(mock(BitmapContext.class))
                .color(Color.RED)
                .initialItem(" SUS ")
                .centered()
                .y(11)
                .build();
        assertNotNull(dut);
    }
    
    // Integration
    @Test
    public void testBuilderChoiceList() {
        ScreenTextList dut = new ScreenTextList.Builder()
                .bitmapContext(mock(BitmapContext.class))
                .inactiveColor(Color.BLACK)
                .activeColor(Color.BLUE)
                .centered()
                .y(15)
                .build();
        assertNotNull(dut);
    }
}
