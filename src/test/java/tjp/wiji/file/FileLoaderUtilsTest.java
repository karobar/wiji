package tjp.wiji.file;

import org.junit.Test;
import tjp.wiji.drawing.bootstrap.AppBootstrapper;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class FileLoaderUtilsTest {
    private static BufferedImage getMinimalImage() throws IOException {
        return FileLoaderUtils.loadPng("sample.png", AppBootstrapper.create().getFilesModule());
    }

    private static BufferedImage getSampleImage() throws IOException {
        return FileLoaderUtils.loadPng("sample_transparency.png", AppBootstrapper.create().getFilesModule());
    }


    @Test
    public void testLoadPng() throws IOException {
        getMinimalImage();
        // smoke test, make sure there's no I/O errors
    }

    @Test
    public void testLoadPngCheckRed() throws IOException {
        BufferedImage image = getMinimalImage();
        Color color = new Color(image.getRGB(0,0));
        assertEquals(Color.RED, color);
    }

    @Test
    public void testLoadPngCheckBlack() throws IOException {
        BufferedImage image = getMinimalImage();
        Color color = new Color(image.getRGB(1,0));
        assertEquals(Color.BLACK, color);
    }

    @Test
    public void testLoadPngCheckAlpha() throws IOException {
        BufferedImage image = getMinimalImage();
        Color color = new Color(image.getRGB(1,1), true);
        assertEquals(new Color(255,255,255,0), color);
    }

    @Test
    public void testSampleImageCheckAlphaArea() throws IOException {
        BufferedImage image = getSampleImage();
        Color color = new Color(image.getRGB(1,1), true);
        assertEquals(new Color(0,0,0,0), color);
    }

    @Test
    public void testSampleImageWhiteArea() throws IOException {
        BufferedImage image = getSampleImage();
        Color color = new Color(image.getRGB(35,32), true);
        assertEquals(Color.WHITE, color);
    }
}