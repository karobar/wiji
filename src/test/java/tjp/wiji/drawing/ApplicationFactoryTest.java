package tjp.wiji.drawing;

import com.google.common.collect.ImmutableSet;
import org.junit.Ignore;
import org.junit.Test;
import tjp.wiji.drawing.bootstrap.AppBootstrapper;
import tjp.wiji.event.GameEvent;
import tjp.wiji.file.IncludedTileset;
import tjp.wiji.gui.gui_element.GuiElement;
import tjp.wiji.gui.screen.Screen;
import tjp.wiji.gui.screen.ScreenChangeController;
import tjp.wiji.gui.screen.ScreenContext;
import tjp.wiji.representations.ImageRepresentation;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Collection;

public class ApplicationFactoryTest {
    static class SampleScreenChangeController implements ScreenChangeController {

        /**
         * Used when exiting a screen to make sure that all pointers decrement by
         * one.
         */
        @Override
        public void stepScreenBackwards() {

        }

        /**
         * used when creating a new screen to make sure that the user can return to
         * the screen they were just at.
         *
         * @param newScreen
         */
        @Override
        public void stepScreenForwards(Screen newScreen) {

        }
    }

    static class SampleScreen extends Screen {
        public SampleScreen(final ScreenChangeController screenChangeController) {
            super(screenChangeController);
        }

        @Override
        protected Collection<GuiElement> getGuiElements() {
            return ImmutableSet.of();
        }

        @Override
        protected ImageRepresentation getCurrentCell(int i, int i1) {
            return ImageRepresentation.ERROR;
        }

        @Override
        protected void handleFrameChange() {

        }

        @Override
        public void stepToScreenTrigger() {

        }

        @Override
        public void handleEvent(final GameEvent gameEvent) {}
    }

    @Ignore
    @Test
    public void testRunEmptyApp() throws URISyntaxException {
        ApplicationFactory.runEmptyApp();
    }

    public static void main(String[] args) throws URISyntaxException, FileNotFoundException, InterruptedException {
        AppBootstrapper bootstrapper = AppBootstrapper.create();
        ApplicationFactory.runApp("WIJI", false, "AppIcon.png",
                        new CellExtents(80, 25),
                BitmapContext.fromEnum(IncludedTileset.DULLARD_EXPONENT_12X12,
                        bootstrapper.getFilesModule()),
                ScreenContext.create(
                        new SampleScreen(new SampleScreenChangeController())));
        Thread.sleep(2000);
    }
}