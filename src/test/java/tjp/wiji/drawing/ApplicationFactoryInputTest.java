package tjp.wiji.drawing;

import com.google.common.collect.ImmutableSet;
import tjp.wiji.drawing.bootstrap.AppBootstrapper;
import tjp.wiji.event.GameEvent;
import tjp.wiji.file.IncludedTileset;
import tjp.wiji.gui.gui_element.GuiElement;
import tjp.wiji.gui.gui_element.NewGuiText;
import tjp.wiji.gui.gui_element.placers.CoordPlacer;
import tjp.wiji.gui.gui_element.placers.Placer;
import tjp.wiji.gui.screen.Screen;
import tjp.wiji.gui.screen.ScreenChangeController;
import tjp.wiji.gui.screen.ScreenContext;
import tjp.wiji.representations.ImageRepresentation;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Collection;

public class ApplicationFactoryInputTest {
    final static class TestInputScreen extends Screen {
        protected TestInputScreen() {
            super(new ApplicationFactoryTest.SampleScreenChangeController());
        }

        @Override
        public void handleEvent(GameEvent e) {
            System.out.println("Test passes!");
        }

        @Override
        protected Collection<GuiElement> getGuiElements() {
            return ImmutableSet.of();
        }

        protected ImmutableSet<Placer> getGuiReps() {
            return ImmutableSet.of(CoordPlacer.centerXandY(new NewGuiText("Press enter to test input")));
        }

        /**
         * @param x horizontal offset from the left edge
         * @param y vertical offset from the top edge
         * @return
         */
        @Override
        protected ImageRepresentation getCurrentCell(int x, int y) {
            return ImageRepresentation.ALL_BLACK;
        }

        @Override
        protected void handleFrameChange() {

        }

        @Override
        public void stepToScreenTrigger() {
            System.out.println("Begin input test...");
        }
    }

    public static void main(String[] args) throws URISyntaxException, FileNotFoundException, InterruptedException {
        final AppBootstrapper bootstrapper = AppBootstrapper.create();
        ApplicationFactory.runApp("Sample", false, "AppIcon.png",
                new CellExtents(80, 25),
                BitmapContext.fromEnum(IncludedTileset.DULLARD_EXPONENT_12X12,
                        bootstrapper.getFilesModule()),
                ScreenContext.create(new TestInputScreen()));
    }
}
