package tjp.wiji.drawing;

import tjp.wiji.drawing.bootstrap.AppBootstrapper;
import tjp.wiji.file.IncludedTileset;
import tjp.wiji.gui.screen.ScreenContext;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;

public class ApplicationFactoryDrawTest {
    public static void main(String[] args) throws URISyntaxException, FileNotFoundException, InterruptedException {
        final AppBootstrapper bootstrapper = AppBootstrapper.create();
        ApplicationFactory.runApp("Sample", false, "AppIcon.png",
                new CellExtents(80, 25),
                BitmapContext.fromEnum(IncludedTileset.DULLARD_EXPONENT_12X12,
                        bootstrapper.getFilesModule()),
                ScreenContext.create(
                        new ApplicationFactoryTest.SampleScreen(new ApplicationFactoryTest.SampleScreenChangeController())));
    }
}
