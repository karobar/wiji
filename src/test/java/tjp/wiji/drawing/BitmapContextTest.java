package tjp.wiji.drawing;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import org.junit.Test;
import tjp.wiji.drawing.bootstrap.AppBootstrapper;
import tjp.wiji.file.FileLoaderUtils;

import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;

public class BitmapContextTest {
    private static final class AppWrapper {
        private final HeadlessApplication delegate;

        public static AppWrapper create() {
            ApplicationListener applicationListener = new ApplicationListener() {
                @Override
                public void create() { }

                @Override
                public void resize(int width, int height) { }

                @Override
                public void render() { }

                @Override
                public void pause() { }

                @Override
                public void resume() { }

                @Override
                public void dispose() { }
            };
            HeadlessApplicationConfiguration conf = new HeadlessApplicationConfiguration();
            return new AppWrapper(
                    new HeadlessApplication(applicationListener, conf));
        }

        private AppWrapper (final HeadlessApplication headlessApplication) {
            this.delegate = headlessApplication;
        }

        Files getFilesModule() {
            return Gdx.files;
        }
    }

    @Test
    public void testBasicLoading() throws IOException, URISyntaxException {
        AppBootstrapper bootstrapper = AppBootstrapper.create();

        final BitmapContext testContext = BitmapContext.fromYaml(
                "Dullard_Exponent_12x12.yaml", bootstrapper.getFilesModule());

        assertEquals(12 ,testContext.getCharPixelHeight());
        assertEquals(12 ,testContext.getCharPixelWidth());
        assertEquals(new Color(0xFF00FF) ,testContext.getControlColor());
        assertEquals(FileLoaderUtils.getFileHandle("Dullard_Exponent_12x12.bmp").file().getCanonicalFile(),
                testContext.getCharsheet().file().getCanonicalFile());
    }
}