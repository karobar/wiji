package tjp.wiji.representations;

import com.google.common.collect.ImmutableList;
import junit.framework.TestCase;

public class ImmutableLayerTest extends TestCase {
    public void testOverlay() {
        ImmutableLayer testOverlay = ImmutableLayer.create(
                new ImageRepresentation('z'));

        ImmutableList<ImageRepresentation> row1 = ImmutableList.of(
                new ImageRepresentation('t'), new ImageRepresentation('e'),
                new ImageRepresentation('s'), new ImageRepresentation('t')
        );
        ImmutableList<ImageRepresentation> row2 = ImmutableList.of(
                new ImageRepresentation('r'), new ImageRepresentation('o'),
                new ImageRepresentation('w'), new ImageRepresentation('2')
        );
        ImmutableLayer testUnderlay = ImmutableLayer.create(
                ImmutableList.of(row1, row2)
        );

        Layer field = testOverlay.overlay(testUnderlay);
        assertEquals(new ImageRepresentation('z'), field.get(0,0).get());
        assertEquals(new ImageRepresentation('e'), field.get(1,0).get());
        assertEquals(new ImageRepresentation('s'), field.get(2,0).get());
        assertEquals(new ImageRepresentation('t'), field.get(3,0).get());
    }

    public void testOverlayWithOffset() {
        ImmutableLayer testOverlay = ImmutableLayer.create(
                new ImageRepresentation('z'));

        ImmutableList<ImageRepresentation> row1 = ImmutableList.of(
                new ImageRepresentation('t'), new ImageRepresentation('e'),
                new ImageRepresentation('s'), new ImageRepresentation('t')
        );
        ImmutableList<ImageRepresentation> row2 = ImmutableList.of(
                new ImageRepresentation('r'), new ImageRepresentation('o'),
                new ImageRepresentation('w'), new ImageRepresentation('2')
        );
        ImmutableLayer testUnderlay = ImmutableLayer.create(
                ImmutableList.of(row1, row2)
        );

        Layer field = testOverlay.overlay(testUnderlay, 1, 1);
        assertEquals(new ImageRepresentation('t'), field.get(0,0).get());
        assertEquals(new ImageRepresentation('e'), field.get(1,0).get());
        assertEquals(new ImageRepresentation('s'), field.get(2,0).get());
        assertEquals(new ImageRepresentation('t'), field.get(3,0).get());
        assertEquals(new ImageRepresentation('r'), field.get(0,1).get());
        assertEquals(new ImageRepresentation('z'), field.get(1,1).get());
        assertEquals(new ImageRepresentation('w'), field.get(2,1).get());
        assertEquals(new ImageRepresentation('2'), field.get(3,1).get());
    }
}