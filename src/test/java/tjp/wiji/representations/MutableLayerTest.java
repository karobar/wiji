package tjp.wiji.representations;

import junit.framework.TestCase;

import java.util.Arrays;
import java.util.List;

public class MutableLayerTest extends TestCase {
    public void testRowConstructor() {
        List<ImageRepresentation> reps = Arrays.asList(
                new ImageRepresentation('h'),
                new ImageRepresentation('i'));

        MutableLayer field = MutableLayer.row(reps);

        assertEquals(new ImageRepresentation('h'), field.get(0,0).get());
        assertEquals(new ImageRepresentation('i'), field.get(1,0).get());
    }
}